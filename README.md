<img src="img/frites_text.png"  width="400">

<img src="img/freight_frenzy.png"  width="400">

## Welcome !

This is the code for Team Frites (UK-148 / 20991), from the 2021-22 Freight Frenzy season.

You'll find the code we wrote in ![TeamCode/src/main/java/org/firstinspires/ftc/teamcode/](https://gitlab.com/ftc-civ/frites/2022/-/tree/main/TeamCode/src/main/java/org/firstinspires/ftc/teamcode/)

The main logic for the autonomous mode is in ![AutoOpMode.java](https://gitlab.com/ftc-civ/frites/2022/-/blob/main/TeamCode/src/main/java/org/firstinspires/ftc/teamcode/auto/AutoOpMode.java) , and in ![ManualOpMode.java](https://gitlab.com/ftc-civ/frites/2022/-/blob/main/TeamCode/src/main/java/org/firstinspires/ftc/teamcode/manual/ManualOpMode.java) for the manual mode. The ![robot folder](https://gitlab.com/ftc-civ/frites/2022/-/tree/main/TeamCode/src/main/java/org/firstinspires/ftc/teamcode/robot) contains all of the shared code for controlling the different parts of the robot.

## Documentation

We have a few documents describing the different parts of the code :

- General overview : https://docs.google.com/presentation/d/1PH8yA4Ot7ahkIBnr8B-EAsQrDT8UOuBNQy6KRFsOGuY/edit?usp=sharing
- Robot classes : https://docs.google.com/presentation/d/12w-Imt4KC35MuaemHxnj2PUobtrZ2nUkJNyTiw2s-G8/edit?usp=sharing
- Autonomous : https://docs.google.com/presentation/d/1fPPAIQAqucywq6BKpZD9DMnoe-js3Oi8PJHmAkkWjjE/edit?usp=sharing
- Manual : https://docs.google.com/presentation/d/1wEP8S1PI2bJirJQl4-fkXS7IA4K4vN14OyL0v1ZvkqM/edit?usp=sharing

You can access our full Freight Frenzy notebook here (code development page) : https://notebook.morethanrobots.uk/user/148/code-development

## Libraries
 
We use a few different external libraries :

#### Road Runner

This is what we use for movement in AutoOp. It allows us to calculate the way our mecanum wheels should move to follow complex trajectories (creating motion profiles), and then follow them using the accelerometer and gyroscope (or another localizer) to correct if necessary.

Official docs : https://acme-robotics.gitbook.io/road-runner/

Learn Road Runner (good ressource for starting) : https://learnroadrunner.com/


#### OpenCV

A widely used computer vision library, which we use for detecting the Team Shipping Element

Website : https://opencv.org/ (not specific to FTC)

FTC EasyOpenCV : https://github.com/OpenFTC/EasyOpenCV (Allows setting up OpenCV quite simply)


#### FTC Dashboard

A really useful tool, allowing you to see telemetry data, field graphs, the camera, and modify config variables from your computer.

Website : https://acmerobotics.github.io/ftc-dashboard/

## License

GNU GPL v3+

This program is free software: you can redistribute it and/or modify it under the terms of the GNU General Public License as published by the Free Software Foundation, either version 3 of the License, or (at your option) any later version.

This program is distributed in the hope that it will be useful, but WITHOUT ANY WARRANTY; without even the implied warranty of MERCHANTABILITY or FITNESS FOR A PARTICULAR PURPOSE. See the GNU General Public License for more details.

You should have received a copy of the GNU General Public License along with this program. If not, see https://www.gnu.org/licenses/. 
