package org.firstinspires.ftc.teamcode.auto;

import android.graphics.Color;

import com.acmerobotics.dashboard.FtcDashboard;
import com.acmerobotics.dashboard.config.Config;
import com.acmerobotics.dashboard.telemetry.MultipleTelemetry;
import com.acmerobotics.roadrunner.geometry.Pose2d;
import com.acmerobotics.roadrunner.geometry.Vector2d;
import com.acmerobotics.roadrunner.trajectory.Trajectory;
import com.acmerobotics.roadrunner.trajectory.TrajectoryBuilder;
import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.hardware.ColorSensor;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorEx;
import com.qualcomm.robotcore.hardware.NormalizedColorSensor;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.robotcore.external.Const;
import org.firstinspires.ftc.robotcore.external.hardware.camera.WebcamName;
import org.firstinspires.ftc.teamcode.roadrunner.drive.MainDrive;
import org.firstinspires.ftc.teamcode.robot.CameraUtils;
import org.firstinspires.ftc.teamcode.robot.Constants;
import org.firstinspires.ftc.teamcode.robot.DropDuck;
import org.firstinspires.ftc.teamcode.robot.GamepadController;
import org.firstinspires.ftc.teamcode.robot.Intake;
import org.firstinspires.ftc.teamcode.robot.Lift;
import org.firstinspires.ftc.teamcode.robot.Movement;

@Config
public class AutoOpMode extends OpMode
{
    /******************** CONFIG ********************/
    public static boolean DEBUG = false;
    public static double HUB_DROP_TIME = 1.5;
    public static double DUCK_TIME = 3.0;
    public static double CAROUSEL_MAX_RUNTIME = 15;
    public static double WAREHOUSE_MAX_RUNTIME = 23;
    public static double INTAKE_FORWARD_STEP = 3;
    public static double MAX_DUCK_DETECTION_TIME = 1.0;
    public static double WAIT_DUCK_TIME = 0.5;

    /******************** CLASS MEMBERS ********************/
    private DropDuck dropduck;
    private FtcDashboard dashboard = FtcDashboard.getInstance();
    private ElapsedTime runtime = new ElapsedTime();
    private GamepadController gamepad;
    private MainDrive drive;
    private CameraUtils cameraUtils;
    private TrajectoryManager trajectoryManager;
    private Intake intake;
    private Lift lift;



    /******************** CLASS PROPERTIES ********************/
    public Constants.Team team;
    
    public enum Mode {
        LOW_HUB_REPEAT_CAROUSEL_STORAGE(0, Constants.StartPosition.LOW),
        // Start from low position, detect duck and drop on hub, get items from warehouse, drop duck from carousel, park in storage

        LOW_HUB_REPEAT_WAREHOUSE(1, Constants.StartPosition.LOW),
        // Start from low position, detect duck and drop on hub, get items from warehouse, park in warehouse

        LOW_HUB_SINGLE_WAREHOUSE(2, Constants.StartPosition.LOW),

        HIGH_HUB_CAROUSEL_STORAGE(3, Constants.StartPosition.HIGH),
        // Start from high position, detect duck and drop on hub, drop duck from carousel, park in storage

        LOW_HUB(4, Constants.StartPosition.LOW),
        // Start from low position, detect duck and drop on hub

        HIGH_HUB(5, Constants.StartPosition.HIGH),
        // Start from high position, detect duck and drop on hub

        IDLE(6, Constants.StartPosition.UNKNOWN);
        // Do absolutely nothing

        public int value;
        public Constants.StartPosition startPosition;

        Mode(int value, Constants.StartPosition startPosition) {
            this.value = value;
            this.startPosition = startPosition;
        }
    }
    public Mode mode;

    private enum State {
        // Trajectories
        TRAJ_STARTLOW_TO_HUBLOW,
        TRAJ_HUBLOW_TO_WAREHOUSE,
        TRAJ_WAREHOUSE_TO_HUBLOW,
        TRAJ_HUBLOW_TO_CAROUSEL,

        TRAJ_STARTHIGH_TO_HUBHIGH,
        TRAJ_HUBHIGH_TO_CAROUSEL,

        TRAJ_CAROUSEL_TO_STORAGE,

        // Actions
        ACTION_INTAKE,
        ACTION_INTAKE_RESET,
        ACTION_DROP_HUB,
        ACTION_CAROUSEL,
        ACTION_DETECT_DUCK,
        ACTION_WAIT_DUCK,

        // Other
        ERROR,
        IDLE
    }
    private State currentState;
    private State previousState;
    private double stateStartTime;

    private double lastRuntime = 0.0;

    private double intakeForward = 0;
    private double intakeStartEncoderPosition = 0;
    private boolean hasAlreadyEnteredWarehouse = false;

    private RuntimeException error;



    /******************** CONSTRUCTORS ********************/
    public AutoOpMode(Constants.Team initTeam, Mode initMode) {
        team = initTeam;
        mode = initMode;
    }



    /******************** INIT ********************/
    @Override
    public void init() {
        ////////// Initialization //////////
        telemetry = new MultipleTelemetry(telemetry, dashboard.getTelemetry());

        telemetry.addLine("Initializing...");

        gamepad = new GamepadController(
                telemetry,
                runtime,
                gamepad1
        );
        telemetry.addLine("Gamepad initialized");

        drive = new MainDrive(hardwareMap);
        telemetry.addLine("Drive initialized");

        cameraUtils = new CameraUtils(
                telemetry,
                runtime,
                team,
                mode.startPosition,
                hardwareMap.get(WebcamName.class, "Webcam 1"),
                hardwareMap.appContext.getResources().getIdentifier("cameraMonitorViewId", "id", hardwareMap.appContext.getPackageName())
        );
        cameraUtils.startWebcam();
        cameraUtils.setInitPipeline();
        telemetry.addLine("Camera initialized");

        intake = new Intake(
                telemetry,
                runtime,
                hardwareMap.get(DcMotorEx.class, "intake_motor"),
                hardwareMap.get(NormalizedColorSensor.class, "color"),
                hardwareMap.get(NormalizedColorSensor.class, "color2")
                );
        telemetry.addLine("Intake initialized");

        lift = new Lift(
                telemetry,
                runtime,
                hardwareMap.get(DcMotorEx.class, "lift_motor"),
                hardwareMap.get(Servo.class, "lift_servo"),
                hardwareMap.get(Servo.class, "lock_servo")
        );
        telemetry.addLine("Lift initialized");

        dropduck = new DropDuck(
                telemetry,
                runtime,
                team,
                hardwareMap.get(DcMotorEx.class, "duck_motor")
        );
        telemetry.addLine("DropDuck initialized");

        ////////// Create trajectories //////////
        trajectoryManager = new TrajectoryManager(drive);
        telemetry.addLine("Trajectories created");

        ////////// Other stuff //////////
        if (mode == Mode.IDLE) {
            currentState = State.IDLE;
        } else {
            currentState = State.ACTION_DETECT_DUCK;
        }
        telemetry.addLine("State set");

        drive.setPoseEstimate(trajectoryManager.getStartPosition(team, mode.startPosition));
        telemetry.addLine("Start position set");

        telemetry.addLine();
        telemetry.addLine("--- INFO ---");
        telemetry.addData("Team", team);
        telemetry.addData("Mode", mode);
        telemetry.addData("StartPosition", mode.startPosition);

        if (DEBUG) {
            telemetry.addLine("DONT FORGET TO DISABLE TIMER");
        }

        telemetry.update();
    }

    @Override
    public void init_loop() {
        if (gamepad.press(GamepadController.Button.A)) {
            cameraUtils.togglePreviewFilter();
        }
        cameraUtils.apply();
        telemetry.addLine();
        telemetry.addLine("--- INFO ---");
        telemetry.addData("Team", team);
        telemetry.addData("Mode", mode);
        telemetry.addData("StartPosition", mode.startPosition);

        if (DEBUG) {
            telemetry.addLine("DONT FORGET TO DISABLE TIMER");
        }
        telemetry.update();
    }

    /******************** UTILS ********************/
    private void setState(State newState) {
        previousState = currentState;
        currentState = newState;
        stateStartTime = runtime.time();
    }

    private boolean stateTimeElapsed(double seconds) {
        return runtime.time() - stateStartTime > seconds;
    }

    private double getEncoderPosition() {
        return Math.abs(drive.getWheelPositions().get(0));
    }


    /******************** LOOP ********************/
    @Override
    public void start() {
        runtime.reset();
        setState(currentState);
        cameraUtils.setMainPipeline();
    }

    @Override
    public void loop() {
        switch (currentState) {
            ////////// Trajectories //////////
            case TRAJ_STARTLOW_TO_HUBLOW:
                if (!drive.isBusy()) {
                    setState(State.ACTION_DROP_HUB);
                }
                break;

            case TRAJ_HUBLOW_TO_WAREHOUSE:
                if (!drive.isBusy()) {
                    intakeStartEncoderPosition = getEncoderPosition();
                    if (mode == Mode.LOW_HUB_REPEAT_WAREHOUSE && runtime.time() >= WAREHOUSE_MAX_RUNTIME) {
                        setState(State.IDLE);
                    } else {
                        setState(State.ACTION_INTAKE);
                    }
                }
                break;

            case TRAJ_WAREHOUSE_TO_HUBLOW:
                if (!drive.isBusy()) {
                    setState(State.ACTION_DROP_HUB);
                }
                break;

            case TRAJ_HUBLOW_TO_CAROUSEL:
                if (!drive.isBusy()) {
                    setState(State.ACTION_CAROUSEL);
                }
                break;

            case TRAJ_STARTHIGH_TO_HUBHIGH:
                if (!drive.isBusy()) {
                    setState(State.ACTION_DROP_HUB);
                }
                break;

            case TRAJ_HUBHIGH_TO_CAROUSEL:
                if (!drive.isBusy()) {
                    setState(State.ACTION_CAROUSEL);
                }
                break;

            case TRAJ_CAROUSEL_TO_STORAGE:
                if (!drive.isBusy()) {
                    setState(State.IDLE);
                }
                break;

            ////////// Actions //////////
            case ACTION_INTAKE:
                intake.setIntake(true);

                // drive.setWeightedDrivePower(new Pose2d(0.3, 0, 0));
                drive.setMotorPowers(0.1, 0.1, 0.1, 0.1);
                // drive.getWheelPositions()
                /* // if (!drive.isBusy()) {
                    if (intake.hasObject()) { // TODO: Change to hasObjectConsistent()
                        intake.rejectForTime();
                        drive.followTrajectoryAsync(drive.trajectoryBuilder(drive.getPoseEstimate()).splineTo(new Vector2d(50, -70), 0).build());


                        setState(State.ACTION_INTAKE_RESET);
                    } else if (!drive.isBusy()) {
                        intakeForward += INTAKE_FORWARD_STEP;
                        drive.forwardAsync(INTAKE_FORWARD_STEP);
                        // drive.waitForIdle(); // NOTE: We're blocking the loop here (this should be an exception). Need to check that this doesn't cause problems
                    }
                // }
                break; */
                if (intake.hasObject()) {
                    intake.rejectForTime();
                    setState(State.ACTION_INTAKE_RESET);
                }

                if (stateTimeElapsed(7)) {
                    setState(State.IDLE);
                }
                break;

            case ACTION_INTAKE_RESET:
                drive.setMotorPowers(-0.5, -0.5, -0.5, -0.5);


                    if ((team == Constants.Team.BLUE && getEncoderPosition() < intakeStartEncoderPosition + 4)
                            || (team == Constants.Team.RED && getEncoderPosition() < intakeStartEncoderPosition + 4)
                    ) {
                        drive.setMotorPowers(0, 0, 0, 0);

                        if ((mode == Mode.LOW_HUB_REPEAT_WAREHOUSE && runtime.time() >= WAREHOUSE_MAX_RUNTIME)
                                || (mode == Mode.LOW_HUB_SINGLE_WAREHOUSE && hasAlreadyEnteredWarehouse)
                        ) {
                            setState(State.IDLE);
                        } else {
                            if (hasAlreadyEnteredWarehouse) {
                                lift.goToPosition(Lift.Position.HUB_LOW);
                            } else {
                                lift.goToPosition(Lift.Position.HUB_MIDDLE);
                            }
                            drive.setPoseEstimate(new Pose2d(45, team == Constants.Team.RED ? -70 : 70, 0)); // HACK: yes, it's ugly, didnt find any other solution

                            drive.followTrajectoryAsync(trajectoryManager.getTrajectory(
                                    team,
                                    TrajectoryManager.AutoTrajectory.WAREHOUSE_TO_HUBLOW
                            ));
                            setState(State.TRAJ_WAREHOUSE_TO_HUBLOW);
                        }
                        hasAlreadyEnteredWarehouse = true;
                    }

                break;

            case ACTION_DROP_HUB:
                lift.unlock();

                if (stateTimeElapsed(HUB_DROP_TIME)) {
                    lift.goToPosition(Lift.Position.DEFAULT);
                    if (mode == Mode.LOW_HUB_REPEAT_WAREHOUSE || mode == Mode.LOW_HUB_SINGLE_WAREHOUSE) {
                        drive.followTrajectoryAsync(trajectoryManager.getTrajectory(
                                team,
                                TrajectoryManager.AutoTrajectory.HUBLOW_TO_WAREHOUSE
                        ));
                        setState(State.TRAJ_HUBLOW_TO_WAREHOUSE);
                    } else if (mode == Mode.LOW_HUB_REPEAT_CAROUSEL_STORAGE) {
                        if (!hasAlreadyEnteredWarehouse) {
                            drive.followTrajectoryAsync(trajectoryManager.getTrajectory(
                                    team,
                                    TrajectoryManager.AutoTrajectory.HUBLOW_TO_WAREHOUSE
                            ));
                            setState(State.TRAJ_HUBLOW_TO_WAREHOUSE);
                        } else {
                            drive.followTrajectoryAsync(trajectoryManager.getTrajectory(
                                    team,
                                    TrajectoryManager.AutoTrajectory.HUBLOW_TO_CAROUSEL
                            ));
                            setState(State.TRAJ_HUBLOW_TO_CAROUSEL);
                        }
                    } else if (mode == Mode.HIGH_HUB_CAROUSEL_STORAGE) {
                        drive.followTrajectoryAsync(trajectoryManager.getTrajectory(
                                team,
                                TrajectoryManager.AutoTrajectory.HUBHIGH_TO_CAROUSEL
                        ));
                        setState(State.TRAJ_HUBHIGH_TO_CAROUSEL);
                    } else {
                        setState(State.IDLE); // Assuming LOW_HUB and HIGH_HUB should stop after having dropped object, tbd
                    }
                }
                break;

            case ACTION_DETECT_DUCK:
                if (cameraUtils.getLiftPosition() != null) {
                    lift.goToPosition(cameraUtils.getLiftPosition());
                    setState(State.ACTION_WAIT_DUCK);
                } else if (stateTimeElapsed(MAX_DUCK_DETECTION_TIME)) {
                    lift.goToPosition(Lift.Position.HUB_LOW);
                    setState(State.ACTION_WAIT_DUCK);
                }
                break;

            case ACTION_WAIT_DUCK:
                if (stateTimeElapsed(WAIT_DUCK_TIME)) {
                    if (mode.startPosition == Constants.StartPosition.LOW) {
                        drive.followTrajectoryAsync(trajectoryManager.getTrajectory(
                                team,
                                TrajectoryManager.AutoTrajectory.STARTLOW_TO_HUBLOW
                        ));
                        setState(State.TRAJ_STARTLOW_TO_HUBLOW);
                    } else if (mode.startPosition == Constants.StartPosition.HIGH) {
                        drive.followTrajectoryAsync(trajectoryManager.getTrajectory(
                                team,
                                TrajectoryManager.AutoTrajectory.STARTHIGH_TO_HUBHIGH
                        ));
                        setState(State.TRAJ_STARTHIGH_TO_HUBHIGH);
                    } else {
                        error = new IllegalThreadStateException("Constants.StartPosition should not be unknown if detecting ducks");
                        setState(State.ERROR);
                    }
                }
                break;

            case ACTION_CAROUSEL:
                dropduck.setDuck(true);

                if (stateTimeElapsed(DUCK_TIME)) {
                    dropduck.setDuck(false);
                    drive.followTrajectoryAsync(trajectoryManager.getTrajectory(
                            team,
                            TrajectoryManager.AutoTrajectory.CAROUSEL_TO_STORAGE
                    ));
                    setState(State.TRAJ_CAROUSEL_TO_STORAGE);
                }
                break;

            ////////// Other //////////
            case ERROR:
                telemetry.addData("Error", error);
                break;

            case IDLE:
                drive.updatePoseEstimate();
                break;
        }

        /* APPLY */
        drive.update();
        lift.apply();
        intake.apply();
        dropduck.apply();
        cameraUtils.apply();

        /* CHORES */
        lastRuntime = runtime.time();
        telemetry.addLine();
        telemetry.addLine("--- MAIN ---");
        telemetry.addData("Current state", currentState);
        telemetry.addData("Mode", mode);
        telemetry.addData("Team", team);
        if (DEBUG) {
            telemetry.addData("Runtime", runtime.time());
            telemetry.addData("stateStartTime", stateStartTime);
            telemetry.addData("intakeStartEncoder", intakeStartEncoderPosition);
            telemetry.addData("encoder", getEncoderPosition());
            telemetry.addData("loop time", runtime.time() - lastRuntime);
            telemetry.addData("error", error);
        }
        telemetry.update();
    }

    @Override
    public void stop() {
        drive.setMotorPowers(0, 0, 0, 0);
        // cameraUtils.stopWebcam();
    }

}
