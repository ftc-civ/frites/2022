/* package org.firstinspires.ftc.teamcode.auto;

import com.acmerobotics.dashboard.FtcDashboard;
import com.acmerobotics.dashboard.config.Config;
import com.acmerobotics.dashboard.telemetry.MultipleTelemetry;
import com.acmerobotics.roadrunner.geometry.Pose2d;
import com.qualcomm.hardware.bosch.BNO055IMU;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorEx;
import com.qualcomm.robotcore.hardware.NormalizedColorSensor;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.robotcore.external.hardware.camera.WebcamName;
import org.firstinspires.ftc.teamcode.roadrunner.drive.MainDrive;
import org.firstinspires.ftc.teamcode.robot.CameraUtils;
import org.firstinspires.ftc.teamcode.robot.DropDuck;
import org.firstinspires.ftc.teamcode.robot.GamepadController;
import org.firstinspires.ftc.teamcode.robot.Intake;
import org.firstinspires.ftc.teamcode.robot.Lift;
import org.firstinspires.ftc.teamcode.robot.Movement;

@Config
public class AutoOpMode_Simple extends OpMode
{
    /******************** CONFIG ********************./
    public static boolean DEBUG = false;
    public static double HUB_DROP_TIME = 2.0;
    public static double DUCK_TIME = 3.0;
    public static double CAROUSEL_MAX_RUNTIME = 20;
    public static double WAREHOUSE_MAX_RUNTIME = 25;
    public static double INTAKE_FORWARD_STEP = 5;
    public static boolean KEEP_ORIENTATION_IMU = true;
    public static double IMU_CORRECTION_P_COEFF = 3.0;
    public static double IMU_CORRECTION_D_COEFF = 0.0;
    // DISTANCES
    public static int STARTLOW_HUB_DISTANCE = 500;
    public static int HUB_MIDPOINT_DISTANCE = 500;
    public static int WAREHOUSE_MIDPOINT_DISTANCE = 300;
    public static int MAX_WAREHOUSE_DISTANCE = 200;

    /******************** CLASS MEMBERS ********************./
    private DropDuck dropduck;
    private FtcDashboard dashboard = FtcDashboard.getInstance();
    private ElapsedTime runtime = new ElapsedTime();
    private GamepadController gamepad;
    private Movement movement;
    private CameraUtils cameraUtils;
    private TrajectoryManager trajectoryManager;
    private Intake intake;
    private Lift lift;



    /******************** CLASS PROPERTIES ********************./
    public AutoOpMode.Team team;

    public enum StartPosition {
        LOW_SIDE(0, 10, 65, Math.toRadians(90), Math.toRadians(-90)),
        HIGH_SIDE(1, -35, 65, Math.toRadians(90), Math.toRadians(-90)),
        UNKNOWN(2, 0, 0, 0, 0);

        public int value;
        public double X;
        public double Y;
        public double blueHeading;
        public double redHeading;

        StartPosition(int value, double X, double Y, double blueHeading, double redHeading) {
            this.value = value;
            this.X = X;
            this.Y = Y;
            this.blueHeading = blueHeading;
            this.redHeading = redHeading;
        }
    }
    public enum Mode {
        LOW_HUB_REPEAT_CAROUSEL_STORAGE(0, StartPosition.LOW_SIDE),
        // Start from low position, detect duck and drop on hub, get items from warehouse, drop duck from carousel, park in storage

        LOW_HUB_REPEAT_WAREHOUSE(1, StartPosition.LOW_SIDE),
        // Start from low position, detect duck and drop on hub, get items from warehouse, park in warehouse

        HIGH_HUB_CAROUSEL_STORAGE(2, StartPosition.HIGH_SIDE),
        // Start from high position, detect duck and drop on hub, drop duck from carousel, park in storage

        LOW_HUB(3, StartPosition.LOW_SIDE),
        // Start from low position, detect duck and drop on hub

        HIGH_HUB(4, StartPosition.HIGH_SIDE),
        // Start from high position, detect duck and drop on hub

        IDLE(5, StartPosition.UNKNOWN);
        // Do absolutely nothing

        public int value;
        public StartPosition startPosition;

        Mode(int value, StartPosition startPosition) {
            this.value = value;
            this.startPosition = startPosition;
        }
    }
    public Mode mode;

    private enum State {
        // Trajectories
        TRAJ_STARTLOW_TO_HUBLOW,
        TRAJ_HUBLOW_TO_WAREHOUSE_1,
        TRAJ_HUBLOW_TO_WAREHOUSE_2,
        TRAJ_WAREHOUSE_TO_HUBLOW_1,
        TRAJ_WAREHOUSE_TO_HUBLOW_2,
        TRAJ_HUBLOW_TO_CAROUSEL,

        TRAJ_STARTHIGH_TO_HUBHIGH,
        TRAJ_HUBHIGH_TO_CAROUSEL,

        TRAJ_CAROUSEL_TO_STORAGE,

        // Actions
        ACTION_INTAKE,
        ACTION_INTAKE_RESET,
        ACTION_DROP_HUB,
        ACTION_CAROUSEL,
        ACTION_DETECT_DUCK,

        // Other
        ERROR,
        IDLE
    }
    private State currentState;
    private State previousState;
    private double stateStartTime;
    private int stateStartEncoder;
    private BNO055IMU imu;

    private double lastRuntime = 0.0;

    private int intakeForward = 0;

    private double initialImuAngle;

    private RuntimeException error;

    private double getImuAngle() {
        return imu.getAngularOrientation().firstAngle;
    }

    /******************** CONSTRUCTORS ********************./
    public AutoOpMode_Simple(AutoOpMode.Team initTeam, Mode initMode) {
        team = initTeam;
        mode = initMode;
    }

    /******************** INIT ********************./
    @Override
    public void init() {
        ////////// Initialization //////////
        telemetry = new MultipleTelemetry(telemetry, dashboard.getTelemetry());

        telemetry.addLine("Initializing...");

        gamepad = new GamepadController(
                telemetry,
                runtime,
                gamepad1
        );
        telemetry.addLine("Gamepad initialized");

        movement = new Movement(
                telemetry,
                runtime,
                hardwareMap.get(DcMotor.class, "front_left_drive"),
                hardwareMap.get(DcMotor.class, "front_right_drive"),
                hardwareMap.get(DcMotor.class, "back_left_drive"),
                hardwareMap.get(DcMotor.class, "back_right_drive"),
                false,
                false
        );
        telemetry.addLine("Movement initialized");

        cameraUtils = new CameraUtils(
                telemetry,
                runtime,
                hardwareMap.get(WebcamName.class, "Webcam 1"),
                hardwareMap.appContext.getResources().getIdentifier("tfodMonitorViewId", "id", hardwareMap.appContext.getPackageName())
        );
        telemetry.addLine("Camera initialized");

        intake = new Intake(
                telemetry,
                runtime,
                hardwareMap.get(DcMotorEx.class, "intake_motor"),
                hardwareMap.get(NormalizedColorSensor.class, "color")
        );
        telemetry.addLine("Intake initialized");

        lift = new Lift(
                telemetry,
                runtime,
                hardwareMap.get(DcMotorEx.class, "lift_motor"),
                hardwareMap.get(Servo.class, "lift_servo"),
                hardwareMap.get(Servo.class, "lock_servo")
        );
        telemetry.addLine("Lift initialized");

        dropduck = new DropDuck(
                telemetry,
                runtime,
                hardwareMap.get(DcMotorEx.class, "duck_motor"),
                team == AutoOpMode.Team.RED
        );
        telemetry.addLine("DropDuck initialized");

        imu = hardwareMap.get(BNO055IMU.class, "imu");
        BNO055IMU.Parameters parameters = new BNO055IMU.Parameters();
        parameters.angleUnit = BNO055IMU.AngleUnit.RADIANS;
        imu.initialize(parameters);
        initialImuAngle = getImuAngle();
        telemetry.addLine("Imu initialized");

        ////////// Other stuff //////////
        if (mode == Mode.IDLE) {
            currentState = State.IDLE;
        } else {
            currentState = State.ACTION_DETECT_DUCK;
        }
        telemetry.addLine("State set");

        telemetry.addLine();
        telemetry.addLine("--- INFO ---");
        telemetry.addData("Team", team);
        telemetry.addData("Mode", mode);
        telemetry.addData("StartPosition", mode.startPosition);

        if (DEBUG) {
            telemetry.addLine("DONT FORGET TO DISABLE TIMER");
        }

        telemetry.update();
    }

    public void init_loop() {

    }

    /******************** UTILS ********************./
    private void setState(State newState) {
        previousState = currentState;
        currentState = newState;
        stateStartTime = runtime.time();
        stateStartEncoder = movement.getEncoderAverage();
    }

    private boolean stateTimeElapsed(double seconds) {
        return runtime.time() - stateStartTime > seconds;
    }

    private boolean stateEncoderMovement(int distance) {
        return Math.abs(movement.getEncoderAverage() - stateStartEncoder) > distance;
    }
    private int stateEncoderMovement() {
        return Math.abs(movement.getEncoderAverage() - stateStartEncoder);
    }


    /******************** LOOP ********************./
    @Override
    public void start() {
        runtime.reset();
        setState(currentState);
    }

    @Override
    public void loop() {
        /* INIT *./
        movement.reset();

        /* LOGIC *./
        switch (currentState) {
            ////////// Trajectories //////////
            case TRAJ_STARTLOW_TO_HUBLOW:
                movement.move(0, 1, 0);
                if (stateEncoderMovement(STARTLOW_HUB_DISTANCE)) {
                    setState(State.ACTION_DROP_HUB);
                }
                break;

            case TRAJ_HUBLOW_TO_WAREHOUSE_1:
                movement.move(0, -1, 0);
                if (stateEncoderMovement(HUB_MIDPOINT_DISTANCE)) {
                    setState(State.TRAJ_HUBLOW_TO_WAREHOUSE_2);
                }
                break;

            case TRAJ_HUBLOW_TO_WAREHOUSE_2:
                movement.move(-1, 0, 0);
                if (stateEncoderMovement(WAREHOUSE_MIDPOINT_DISTANCE)) {
                    if (mode == Mode.LOW_HUB_REPEAT_WAREHOUSE && runtime.time() >= WAREHOUSE_MAX_RUNTIME) {
                        setState(State.IDLE);
                    } else {
                        setState(State.ACTION_INTAKE);
                    }
                }
                break;

            case TRAJ_WAREHOUSE_TO_HUBLOW_1:
                movement.move(1, 0, 0);
                if (stateEncoderMovement(WAREHOUSE_MIDPOINT_DISTANCE)) {
                    setState(State.TRAJ_WAREHOUSE_TO_HUBLOW_2);
                }
                break;

            case TRAJ_WAREHOUSE_TO_HUBLOW_2:
                movement.move(0, 1, 0);
                if (stateEncoderMovement(HUB_MIDPOINT_DISTANCE)) {
                    setState(State.ACTION_DROP_HUB);
                }

            case TRAJ_HUBLOW_TO_CAROUSEL:
                // TODO
                /* if (!drive.isBusy()) {
                    setState(State.ACTION_CAROUSEL);
                } *./
                break;

            case TRAJ_STARTHIGH_TO_HUBHIGH:
                // TODO
                /*if (!drive.isBusy()) {
                    setState(State.ACTION_DROP_HUB);
                }*./
                break;

            case TRAJ_HUBHIGH_TO_CAROUSEL:
                // TODO
                /* if (!drive.isBusy()) {
                    setState(State.ACTION_CAROUSEL);
                }*./
                break;

            case TRAJ_CAROUSEL_TO_STORAGE:
                /* if (!drive.isBusy()) {
                    setState(State.IDLE);
                } *./
                break;

            ////////// Actions //////////
            case ACTION_INTAKE:
                intake.setIntake(true);
                if (!stateEncoderMovement(MAX_WAREHOUSE_DISTANCE)) {
                    movement.move(0.2, 0, 0);
                } else {
                    // TODO: We have a problem
                }

                    if (intake.hasObject()) { // TODO: Change to hasObjectConsistent()
                        intake.setIntake(false);
                        intake.rejectForTime();
                        intakeForward = stateEncoderMovement();
                        setState(State.ACTION_INTAKE_RESET);
                    }
                break;

            case ACTION_INTAKE_RESET:
                movement.move(-1, 0, 0);
                if (stateEncoderMovement(intakeForward)) {
                    if (intake.getCurrentObject() == Intake.IntakeObject.BALL) {
                        lift.goToPosition(Lift.Position.HUB_HIGH);
                    } else {
                        lift.goToPosition(Lift.Position.HUB_LOW);
                    }

                    setState(State.TRAJ_WAREHOUSE_TO_HUBLOW_1);
                }
                break;

            case ACTION_DROP_HUB:
                lift.unlock();

                if (stateTimeElapsed(HUB_DROP_TIME)) {
                    lift.goToPosition(Lift.Position.DEFAULT);
                    if (mode == Mode.LOW_HUB_REPEAT_WAREHOUSE) {
                        setState(State.TRAJ_HUBLOW_TO_WAREHOUSE_1);
                    } else if (mode == Mode.LOW_HUB_REPEAT_CAROUSEL_STORAGE) {
                        if (runtime.time() < CAROUSEL_MAX_RUNTIME) {
                            setState(State.TRAJ_HUBLOW_TO_WAREHOUSE_1);
                        } else {
                            setState(State.TRAJ_HUBLOW_TO_CAROUSEL);
                        }
                    } else if (mode == Mode.HIGH_HUB_CAROUSEL_STORAGE) {
                        setState(State.TRAJ_HUBHIGH_TO_CAROUSEL);
                    } else {
                        setState(State.IDLE); // Assuming LOW_HUB and HIGH_HUB should stop after having dropped object, tbd
                    }
                }
                break;

            case ACTION_DETECT_DUCK:
                lift.goToPosition(Lift.Position.HUB_HIGH); // TODO: Detect duck

                if (mode.startPosition == StartPosition.LOW_SIDE) {
                    setState(State.TRAJ_STARTLOW_TO_HUBLOW);
                } else if (mode.startPosition == StartPosition.HIGH_SIDE) {
                    setState(State.TRAJ_STARTHIGH_TO_HUBHIGH);
                } else {
                    error = new IllegalThreadStateException("StartPosition should not be unknown if detecting ducks");
                    setState(State.ERROR);
                }
                break;

            case ACTION_CAROUSEL:
                dropduck.setDuck(true);

                if (stateTimeElapsed(DUCK_TIME)) {
                    dropduck.setDuck(false);
                    setState(State.TRAJ_CAROUSEL_TO_STORAGE);
                }
                break;

            ////////// Other //////////
            case ERROR:
                telemetry.addLine("CRITICAL ERROR OCCURED");
                telemetry.addData("Error", error);
                break;

            case IDLE:
                break;
        }

        /* IMU CORRECTION *./
        // TODO: Add a proper Proportional/Derivative controller here
        if (KEEP_ORIENTATION_IMU) {
            movement.move(0, 0, getImuAngle() - initialImuAngle / (Math.PI * 2 * IMU_CORRECTION_P_COEFF));
        }

        /* APPLY *./
        movement.apply();
        lift.apply();
        intake.apply();
        dropduck.apply();

        /* CHORES *./
        lastRuntime = runtime.time();
        telemetry.addLine();
        telemetry.addLine("--- MAIN ---");
        telemetry.addData("Current state", currentState);
        telemetry.addData("Mode", mode);
        telemetry.addData("Team", team);
        telemetry.addData("Imu angle", getImuAngle() - initialImuAngle);
        telemetry.addData("Imu raw (first angle)", imu.getAngularOrientation().firstAngle);
        telemetry.addData("Imu raw (second angle)", imu.getAngularOrientation().secondAngle);
        telemetry.addData("Imu raw (third angle)", imu.getAngularOrientation().thirdAngle);
        if (DEBUG) {
            telemetry.addData("Runtime", runtime.time());
            telemetry.addData("stateStartTime", stateStartTime);
            telemetry.addData("loop time", runtime.time() - lastRuntime);
            telemetry.addData("error", error);
        }
        telemetry.update();
    }


}*/
