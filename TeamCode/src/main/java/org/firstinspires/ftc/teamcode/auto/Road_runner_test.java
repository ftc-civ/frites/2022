package org.firstinspires.ftc.teamcode.auto;

import com.acmerobotics.dashboard.config.Config;
import com.acmerobotics.roadrunner.geometry.Pose2d;
import com.acmerobotics.roadrunner.geometry.Vector2d;
import com.acmerobotics.roadrunner.trajectory.Trajectory;
import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotorEx;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.teamcode.roadrunner.drive.MainDrive;
import org.firstinspires.ftc.teamcode.robot.Constants;
import org.firstinspires.ftc.teamcode.robot.DropDuck;
import org.firstinspires.ftc.teamcode.robot.Lift;

@Autonomous(name = "road_runner_test rouge")
@Config
public class Road_runner_test extends LinearOpMode {
    public Constants.Team team;
    private MainDrive drive;
    private Lift lift;
    private ElapsedTime runtime=new ElapsedTime();
    private DropDuck dropduck;


    @Override
    public void runOpMode(){
        dropduck = new DropDuck(
                telemetry,
                runtime,
                team,
                hardwareMap.get(DcMotorEx.class, "duck_motor") );
        lift = new Lift(
            telemetry,
            runtime,
            hardwareMap.get(DcMotorEx.class, "lift_motor"),
            hardwareMap.get(Servo .class, "lift_servo"),
            hardwareMap.get(Servo.class, "lock_servo") );

        drive = new MainDrive(hardwareMap);
        drive.setPoseEstimate(new Pose2d(-12, -12, Math.toRadians(180)));
        Trajectory main = drive.trajectoryBuilder(new Pose2d(-12, -12, Math.toRadians(180)))
                .splineTo(new Vector2d(-53.1, -24.6), Math.toRadians(270))
                .splineTo(new Vector2d(-12, -51.2), 0)
                .splineTo(new Vector2d(12, -43.3), Math.toRadians(50))
                .splineToSplineHeading(new Pose2d(60, 12, Math.toRadians(220)), Math.toRadians(50))
                .splineTo(new Vector2d(65,40), Math.toRadians(90))
                .build();

        waitForStart();

        drive.followTrajectory(main);

        lift.goToPosition(Lift.Position.HUB_HIGH);
        while (!lift.isAtTargetPosition()) {
            lift.apply();
        }
        lift.unlock();
        lift.apply();

        double startTime = runtime.time();
        while (runtime.time() - time < 3);


    }
}
