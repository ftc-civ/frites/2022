package org.firstinspires.ftc.teamcode.auto;

import com.acmerobotics.dashboard.config.Config;
import com.acmerobotics.roadrunner.geometry.Pose2d;
import com.acmerobotics.roadrunner.geometry.Vector2d;
import com.acmerobotics.roadrunner.trajectory.Trajectory;
import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.DcMotorEx;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.teamcode.roadrunner.drive.MainDrive;
import org.firstinspires.ftc.teamcode.robot.Constants;
import org.firstinspires.ftc.teamcode.robot.DropDuck;
import org.firstinspires.ftc.teamcode.robot.Lift;

@Autonomous(name = "road_runner_test bleu")
@Config
public class Road_runner_test_blue extends LinearOpMode {
   // public Constants.Team team;
    private MainDrive drive;
    private Lift lift;
    private ElapsedTime runtime=new ElapsedTime();
   // private DropDuck dropduck;



    @Override
    public void runOpMode(){
        lift = new Lift(
            telemetry,
            runtime,
            hardwareMap.get(DcMotorEx.class, "lift_motor"),
            hardwareMap.get(Servo .class, "lift_servo"),
            hardwareMap.get(Servo.class, "lock_servo") );

        dropduck = new DropDuck(
                telemetry,
                runtime,
                team,
                hardwareMap.get(DcMotorEx.class, "duck_motor")

        drive = new MainDrive(hardwareMap);
        drive.setPoseEstimate(new Pose2d(-12, 12, Math.toRadians(180)));
        Trajectory main = drive.trajectoryBuilder(new Pose2d(-12, 12, Math.toRadians(180)))
                .splineTo(new Vector2d(-53.1, 24.6), Math.toRadians(90))
                .splineTo(new Vector2d(-12, 51.2), 0)
                .splineTo(new Vector2d(12, 43.3), Math.toRadians(330))
                .splineToSplineHeading(new Pose2d(60, -12, Math.toRadians(180)), Math.toRadians(330))
                .splineTo(new Vector2d(65,-40), Math.toRadians(220))
                .build();

        /*Trajectory main = drive.trajectoryBuilder(new Pose2d(-12, 12, Math.toRadians(180)))
                .splineTo(new Vector2d(-55.1, 24.6), Math.toRadians(90))
                .splineTo(new Vector2d(-55.1, 59.1), 0)
                .build();
        dropduck.getDuck();

        Trajectory main = drive.trajectoryBuilder(new Pose2d(-12, 12, Math.toRadians(180)))

        dropduck.apply();*/




        waitForStart();

        drive.followTrajectory(main);

        lift.goToPosition(Lift.Position.HUB_HIGH);
        while (!lift.isAtTargetPosition()) {
            lift.apply();
        }
        lift.unlock();
        lift.apply();

        double startTime = runtime.time();
        while (runtime.time() - time < 3);

        lift.goToPosition(Lift.Position.DEFAULT);
        while (!lift.isAtTargetPosition()) {
            lift.apply();
        }


    }
}
