package org.firstinspires.ftc.teamcode.auto;

import com.acmerobotics.dashboard.config.Config;
import com.acmerobotics.roadrunner.geometry.Pose2d;
import com.acmerobotics.roadrunner.geometry.Vector2d;
import com.acmerobotics.roadrunner.trajectory.Trajectory;

import org.firstinspires.ftc.teamcode.roadrunner.drive.MainDrive;
import org.firstinspires.ftc.teamcode.robot.Constants;

@Config
public class TrajectoryManager {

    // Trajectories
    private Trajectory traj_blue_startlow_to_hublow;
    private Trajectory traj_blue_hublow_to_warehouse;
    private Trajectory traj_blue_warehouse_to_hublow;
    private Trajectory traj_blue_hublow_to_carousel;
    private Trajectory traj_blue_starthigh_to_hubhigh;
    private Trajectory traj_blue_hubhigh_to_carousel;
    private Trajectory traj_blue_carousel_to_storage;
    private Trajectory traj_blue_warehouse_backandforth;

    private Trajectory traj_red_startlow_to_hublow;
    private Trajectory traj_red_hublow_to_warehouse;
    private Trajectory traj_red_warehouse_to_hublow;
    private Trajectory traj_red_hublow_to_carousel;
    private Trajectory traj_red_starthigh_to_hubhigh;
    private Trajectory traj_red_hubhigh_to_carousel;
    private Trajectory traj_red_carousel_to_storage;
    private Trajectory traj_red_warehouse_backandforth;

    // Enum
    public enum AutoTrajectory {
        STARTLOW_TO_HUBLOW,
        HUBLOW_TO_WAREHOUSE,
        WAREHOUSE_TO_HUBLOW,
        HUBLOW_TO_CAROUSEL,
        STARTHIGH_TO_HUBHIGH,
        HUBHIGH_TO_CAROUSEL,
        CAROUSEL_TO_STORAGE,
    }

    public TrajectoryManager(MainDrive drive) {
        // Note : For red, y is negative
        traj_red_startlow_to_hublow = drive.trajectoryBuilder(new Pose2d(10, -65, 0))
                .lineToSplineHeading(new Pose2d( 10, -20, 0))
                .build();
        traj_blue_startlow_to_hublow = drive.trajectoryBuilder(new Pose2d(10, 65, 0))
                .lineToSplineHeading(new Pose2d( 10, 20, 0))
                .build();

        traj_red_hublow_to_warehouse = drive.trajectoryBuilder(new Pose2d(10, -20, 0))
                .lineToConstantHeading(new Vector2d(10, -60))
                .splineToConstantHeading(new Vector2d(15, -70), 0)
                .lineToConstantHeading(new Vector2d(50, -70))
                .build();
        traj_blue_hublow_to_warehouse = drive.trajectoryBuilder(new Pose2d(10, 20, 0))
                .lineToConstantHeading(new Vector2d(10, 60))
                .splineToConstantHeading(new Vector2d(15, 70), 0)
                .lineToConstantHeading(new Vector2d(50, 70))
                .build();

        traj_red_warehouse_to_hublow = drive.trajectoryBuilder(new Pose2d(50, -70, 0))
                .lineToConstantHeading(new Vector2d(15, -70))
                .splineToConstantHeading(new Vector2d(10, -60), Math.toRadians(90))
                .lineToConstantHeading(new Vector2d(10, -20) ) // For some reason lineToConstantHeading refuses to work here TODO: try changing endtangent on first spline
                .build();
        traj_blue_warehouse_to_hublow = drive.trajectoryBuilder(new Pose2d(50, 70, 0))
                .lineToConstantHeading(new Vector2d(15, 70))
                .splineToConstantHeading(new Vector2d(10, 60), Math.toRadians(-90))
                .lineToConstantHeading(new Vector2d(10, 20)) // For some reason lineToConstantHeading refuses to work here TODO: try changing endtangent on first spline
                .build();

        traj_red_hublow_to_carousel = drive.trajectoryBuilder(new Pose2d(10, -20, 0))
                .lineToConstantHeading(new Vector2d(10, 0))
                .splineTo(new Vector2d(-30, 0), Math.toRadians(180))
                .splineTo(new Vector2d(-70, -60), Math.toRadians(-90))
                .lineToConstantHeading(new Vector2d(-70, -75))
                .build();
        traj_blue_hublow_to_carousel = drive.trajectoryBuilder(new Pose2d(10, 20, 0))
                .lineToConstantHeading(new Vector2d(10, -5))
                .splineTo(new Vector2d(-30, -5), Math.toRadians(180))
                .splineToSplineHeading(new Pose2d(-75, 40, Math.toRadians(90)), Math.toRadians(90))
                .lineToConstantHeading(new Vector2d(-75, 50))
                .build();

        traj_red_starthigh_to_hubhigh = drive.trajectoryBuilder(new Pose2d(-35, -65, Math.toRadians(180)))
                .lineToSplineHeading(new Pose2d(-35, -17, Math.toRadians(180)))
                .build();
        traj_blue_starthigh_to_hubhigh = drive.trajectoryBuilder(new Pose2d(-35, 65, Math.toRadians(180)))
                .lineToSplineHeading(new Pose2d(-35, 17, Math.toRadians(180)))
                .build();

        traj_red_hubhigh_to_carousel = drive.trajectoryBuilder(new Pose2d(-35, -17, Math.toRadians(180)))
                .lineToSplineHeading(new Pose2d(-70, -67, Math.toRadians(180)))
                .build();
        traj_blue_hubhigh_to_carousel = drive.trajectoryBuilder(new Pose2d(-35, 17, Math.toRadians(180)))
                .splineTo(new Vector2d(-70, 50), Math.toRadians(90))
                .build();

        traj_red_carousel_to_storage = drive.trajectoryBuilder(new Pose2d(-70, -70, Math.toRadians(180)))
                .lineToSplineHeading(new Pose2d(-70, -45, Math.toRadians(-90)))
                .build();
        traj_blue_carousel_to_storage = drive.trajectoryBuilder(new Pose2d(-70, 50, Math.toRadians(90)))
                .lineToConstantHeading(new Vector2d(-70, 30))
                .build();
    }

    public Trajectory getTrajectory(Constants.Team team, AutoTrajectory autoTrajectory) {
        switch (autoTrajectory) {
            case STARTLOW_TO_HUBLOW:
                return team == Constants.Team.BLUE ? traj_blue_startlow_to_hublow : traj_red_startlow_to_hublow;
            case HUBLOW_TO_WAREHOUSE:
                return team == Constants.Team.BLUE ? traj_blue_hublow_to_warehouse : traj_red_hublow_to_warehouse;
            case WAREHOUSE_TO_HUBLOW:
                return team == Constants.Team.BLUE ? traj_blue_warehouse_to_hublow : traj_red_warehouse_to_hublow;
            case HUBLOW_TO_CAROUSEL:
                return team == Constants.Team.BLUE ? traj_blue_hublow_to_carousel : traj_red_hublow_to_carousel;
            case STARTHIGH_TO_HUBHIGH:
                return team == Constants.Team.BLUE ? traj_blue_starthigh_to_hubhigh : traj_red_starthigh_to_hubhigh;
            case HUBHIGH_TO_CAROUSEL:
                return team == Constants.Team.BLUE ? traj_blue_hubhigh_to_carousel : traj_red_hubhigh_to_carousel;
            case CAROUSEL_TO_STORAGE:
                return team == Constants.Team.BLUE ? traj_blue_carousel_to_storage : traj_red_carousel_to_storage;
            default:
                throw new IllegalThreadStateException("Unknown trajectory to get");
        }
    }

    public Pose2d getStartPosition(Constants.Team team, Constants.StartPosition startPosition) {
        if (startPosition == Constants.StartPosition.UNKNOWN) {
            throw new IllegalThreadStateException("Cannot get start pose with unknown position");
        }
        if (team == Constants.Team.BLUE) {
            if (startPosition == Constants.StartPosition.HIGH) {
                return new Pose2d(-35, 65, Math.toRadians(180));
            } else {
                return new Pose2d(10, 65, 0);
            }
        } else {
            if (startPosition == Constants.StartPosition.HIGH) {
                return new Pose2d(-35, -65, Math.toRadians(180));
            } else {
                return new Pose2d(10, -65, 0);
            }
        }
    }
}
