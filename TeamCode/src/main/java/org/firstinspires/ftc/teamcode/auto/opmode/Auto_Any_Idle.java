package org.firstinspires.ftc.teamcode.auto.opmode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;

import org.firstinspires.ftc.teamcode.auto.AutoOpMode;
import org.firstinspires.ftc.teamcode.robot.Constants;

@Autonomous(name="Any Idle", group="Other")
public class Auto_Any_Idle extends AutoOpMode {
    public Auto_Any_Idle() {
        super(Constants.Team.RED, Mode.IDLE);
        this.msStuckDetectInit = 30000;
    }
}
