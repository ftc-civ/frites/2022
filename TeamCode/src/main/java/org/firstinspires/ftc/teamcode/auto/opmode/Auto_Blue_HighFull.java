package org.firstinspires.ftc.teamcode.auto.opmode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;

import org.firstinspires.ftc.teamcode.auto.AutoOpMode;
import org.firstinspires.ftc.teamcode.robot.Constants;

@Autonomous(name="Blue High Full", group="Blue")
public class Auto_Blue_HighFull extends AutoOpMode {
    public Auto_Blue_HighFull() {
        super(Constants.Team.BLUE, Mode.HIGH_HUB_CAROUSEL_STORAGE);
        this.msStuckDetectInit = 30000;
    }
}
