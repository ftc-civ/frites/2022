package org.firstinspires.ftc.teamcode.auto.opmode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;

import org.firstinspires.ftc.teamcode.auto.AutoOpMode;
import org.firstinspires.ftc.teamcode.robot.Constants;

@Autonomous(name="Blue High HubOnly", group="Blue")
public class Auto_Blue_HighHub extends AutoOpMode {
    public Auto_Blue_HighHub() {
        super(Constants.Team.BLUE, Mode.HIGH_HUB);
        this.msStuckDetectInit = 30000;
    }
}
