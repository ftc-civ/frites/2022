package org.firstinspires.ftc.teamcode.auto.opmode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;

import org.firstinspires.ftc.teamcode.auto.AutoOpMode;
import org.firstinspires.ftc.teamcode.robot.Constants;

@Autonomous(name="Blue Low Full", group="Blue")
public class Auto_Blue_LowFull extends AutoOpMode {
    public Auto_Blue_LowFull() {
        super(Constants.Team.BLUE, Mode.LOW_HUB_REPEAT_CAROUSEL_STORAGE);
        this.msStuckDetectInit = 30000;
    }
}
