package org.firstinspires.ftc.teamcode.auto.opmode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;

import org.firstinspires.ftc.teamcode.auto.AutoOpMode;
import org.firstinspires.ftc.teamcode.robot.Constants;

@Autonomous(name="Blue Low HubOnly", group="Blue")
public class Auto_Blue_LowHub extends AutoOpMode {
    public Auto_Blue_LowHub() {
        super(Constants.Team.BLUE, Mode.LOW_HUB);
        this.msStuckDetectInit = 30000;
    }
}
