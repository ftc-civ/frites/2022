package org.firstinspires.ftc.teamcode.auto.opmode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;

import org.firstinspires.ftc.teamcode.auto.AutoOpMode;
import org.firstinspires.ftc.teamcode.robot.Constants;

@Autonomous(name="Blue Low Single Warehouse", group="Blue")
public class Auto_Blue_LowSingleWarehouse extends AutoOpMode {
    public Auto_Blue_LowSingleWarehouse() {
        super(Constants.Team.BLUE, Mode.LOW_HUB_SINGLE_WAREHOUSE);
        this.msStuckDetectInit = 30000;
    }
}
