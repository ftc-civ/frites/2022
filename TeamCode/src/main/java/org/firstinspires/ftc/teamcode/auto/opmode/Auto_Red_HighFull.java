package org.firstinspires.ftc.teamcode.auto.opmode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;

import org.firstinspires.ftc.teamcode.auto.AutoOpMode;
import org.firstinspires.ftc.teamcode.robot.Constants;

@Autonomous(name="Red High Full", group="Red")
public class Auto_Red_HighFull extends AutoOpMode {
    public Auto_Red_HighFull() {
        super(Constants.Team.RED, Mode.HIGH_HUB_CAROUSEL_STORAGE);
        this.msStuckDetectInit = 30000;
    }
}
