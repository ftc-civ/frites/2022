package org.firstinspires.ftc.teamcode.auto.opmode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;

import org.firstinspires.ftc.teamcode.auto.AutoOpMode;
import org.firstinspires.ftc.teamcode.robot.Constants;

@Autonomous(name="Red High HubOnly", group="Red")
public class Auto_Red_HighHub extends AutoOpMode {
    public Auto_Red_HighHub() {
        super(Constants.Team.RED, Mode.HIGH_HUB);
        this.msStuckDetectInit = 30000;
    }
}
