package org.firstinspires.ftc.teamcode.auto.opmode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;

import org.firstinspires.ftc.teamcode.auto.AutoOpMode;
import org.firstinspires.ftc.teamcode.robot.Constants;

@Autonomous(name="Red Low Full", group="Red")
public class Auto_Red_LowFull extends AutoOpMode {
    public Auto_Red_LowFull() {
        super(Constants.Team.RED, Mode.LOW_HUB_REPEAT_CAROUSEL_STORAGE);
        this.msStuckDetectInit = 30000;
    }
}
