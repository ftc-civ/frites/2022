package org.firstinspires.ftc.teamcode.auto.opmode;

import com.qualcomm.robotcore.eventloop.opmode.Autonomous;

import org.firstinspires.ftc.teamcode.auto.AutoOpMode;
import org.firstinspires.ftc.teamcode.robot.Constants;

@Autonomous(name="Red Low Repeat Warehouse", group="Red")
public class Auto_Red_LowRepeatWarehouse extends AutoOpMode {
    public Auto_Red_LowRepeatWarehouse() {
        super(Constants.Team.RED, Mode.LOW_HUB_REPEAT_WAREHOUSE);
        this.msStuckDetectInit = 30000;
    }
}
