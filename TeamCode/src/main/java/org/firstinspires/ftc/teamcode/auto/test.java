package org.firstinspires.ftc.teamcode.auto;

import com.acmerobotics.dashboard.FtcDashboard;
import com.acmerobotics.dashboard.config.Config;
import com.acmerobotics.dashboard.telemetry.MultipleTelemetry;
import com.qualcomm.robotcore.eventloop.opmode.Autonomous;
import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.hardware.ColorSensor;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorEx;
import com.qualcomm.robotcore.hardware.DistanceSensor;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.robotcore.external.navigation.DistanceUnit;
import org.firstinspires.ftc.teamcode.robot.Lift;
import org.firstinspires.ftc.teamcode.robot.Movement;

@Autonomous(name="Test autonome")
@Config
public class test extends LinearOpMode {
    private DistanceSensor capteurAvant;
    private ColorSensor capteurGauche;
    private ColorSensor capteurDroite;
    private Movement movement;
    private Lift lift;
    private ElapsedTime runtime=new ElapsedTime();
    private FtcDashboard dashboard = FtcDashboard.getInstance();

    public static double setPoint = 400; // La valeur ou on veut aller (le gris)
    public static double kp = 0.0002; // Le coefficient proportionnel
    public static double kd = 0; // Le coefficient dérivée
    public static double vitesse = 0.4; // La vitesse du robot

    @Override
    public void runOpMode() {
        telemetry = new MultipleTelemetry(telemetry, dashboard.getTelemetry());
        lift = new Lift(
                telemetry,
                runtime,
                hardwareMap.get(DcMotorEx.class, "lift_motor"),
                hardwareMap.get(Servo.class, "lift_servo"),
                hardwareMap.get(Servo.class, "lock_servo") );

        movement = new Movement(
                telemetry,
                runtime,
                hardwareMap.get(DcMotor.class, "front_left_drive"),
                hardwareMap.get(DcMotor.class, "front_right_drive"),
                hardwareMap.get(DcMotor.class, "back_left_drive"),
                hardwareMap.get(DcMotor.class, "back_right_drive"),
                false,
                true
        );
        capteurAvant = hardwareMap.get(DistanceSensor.class, "capteur_avant");
        capteurGauche = hardwareMap.get(ColorSensor.class, "capteur_couleur_gauche");
        capteurDroite = hardwareMap.get(ColorSensor.class, "capteur_couleur_droit");

        waitForStart();
        runtime.reset();

        double error;
        double prevError = 0;
        double time = runtime.time();

        while (opModeIsActive()){
            double capteur_avant = capteurAvant.getDistance(DistanceUnit.CM);
            double capteurGaucheBlanc = 0.2 * capteurGauche.alpha() - 60;
            double capteurDroiteBlanc = capteurDroite.alpha();
            double capteurGaucheRouge = 0.2 * capteurGauche.red() - 60;
            double capteurDroiteRouge = capteurDroite.red();
            double capteurGaucheBleu = 0.2 * capteurGauche.blue() - 60;
            double capteurDroiteBleu = capteurDroite.blue();
            int turnings = 0;

            movement.reset();

            /*while (turnings <= 2) {
                while (capteur_avant >= 30) {
                    movement.move(0.5, 0,0);
                }
                movement.move(0, 0, 90);
                turnings =+ 1;
            }*/




            /*error = (setPoint - capteurGaucheBlanc) - (setPoint - capteurDroiteBlanc); // Pour l'erreur, on dit que si c'est positif on est trop à gauche, si c'est négatif on est trop à droite
            double derivee = (prevError - error) / (time - runtime.time()); // Une implémentation assez naive de la dérivée, on prends juste la valeur précédente (idéalement, il faudrait le faire sur trois ou quatre valeurs)
            double valeur = error * kp + derivee * kd; // Le controller PID en lui-meme
            movement.move(vitesse, 0, valeur);
            prevError = error;
            time = runtime.time();*/

            if(capteurGaucheBlanc>1000) {
                movement.move(0.3, 0, -0.3);
            }
            else if(capteurDroiteBlanc>1000) {
                movement.move(0.3, 0, 0.3);
            }
            else {
                movement.move(0.5, 0, 0);
            }
            movement.apply();


            movement.apply();

            //telemetry.addData("error", error);
            //telemetry.addData("valeur", valeur);
            telemetry.addData("Vitesse", vitesse);
            telemetry.addData("capteur_gauche_red", capteurGaucheRouge);
            telemetry.addData("capteur_gauche_blue", capteurGaucheBleu);
            telemetry.addData("capteur_gauche_white", capteurGaucheBlanc);
            telemetry.addData("capteur_droit_red", capteurDroiteRouge);
            telemetry.addData("capteur_droit_blue", capteurDroiteBleu);
            telemetry.addData("capteur_droit_white", capteurDroiteBlanc);
            telemetry.update();

            if ((capteurGaucheBleu < 150 && capteurGaucheRouge < 500)|| (capteurDroiteBleu < 150 && capteurDroiteRouge < 500))  {
                movement.move(0, 0, 0);
            }
            lift.goToPosition(Lift.Position.HUB_HIGH);
            lift.apply();


        }

    }
}
