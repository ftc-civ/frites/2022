package org.firstinspires.ftc.teamcode.manual;

import com.acmerobotics.dashboard.FtcDashboard;
import com.acmerobotics.dashboard.config.Config;
import com.acmerobotics.dashboard.telemetry.MultipleTelemetry;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.ColorSensor;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorEx;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.NormalizedColorSensor;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.ElapsedTime;


import org.firstinspires.ftc.robotcore.external.hardware.camera.WebcamName;
import org.firstinspires.ftc.teamcode.manual.opmode.Manual_Blue;
import org.firstinspires.ftc.teamcode.robot.CameraUtils;
import org.firstinspires.ftc.teamcode.robot.Constants;
import org.firstinspires.ftc.teamcode.robot.DropDuck;
import org.firstinspires.ftc.teamcode.robot.GamepadController;
import org.firstinspires.ftc.teamcode.robot.Intake;
import org.firstinspires.ftc.teamcode.robot.Lift;
import org.firstinspires.ftc.teamcode.robot.Movement;

import static java.lang.Thread.sleep;

import java.util.concurrent.TimeUnit;

@Config
public class ManualOpMode extends OpMode
{
    public Constants.Team team;
    //////////////////////////////////////////////////////// CLASS MEMBERS /////////////////////////////////////////////////////////
    public static boolean DEBUG = false;
    public static boolean AUTOMATION_ENABLED_DEFAULT = true;
    public static boolean MOVEMENT_ENABLED_DEFAULT = true;
    public static boolean MOVEMENT_BRAKE = false;

    private DropDuck dropduck;
    private FtcDashboard dashboard = FtcDashboard.getInstance();
    private ElapsedTime runtime = new ElapsedTime();
    private Movement movement;
    private GamepadController gamepad;
    private CameraUtils cameraUtils;
    private Intake intake;
    private Lift lift;
    private boolean hasProcessedObject = false;
    private boolean isWaitingForLiftReset = false;
    private boolean sharedHubMode = false;
    private Lift.Position cubeLiftPosition = Lift.Position.HUB_HIGH;
    private boolean automationEnabled = AUTOMATION_ENABLED_DEFAULT;
    public boolean movementEnabled = MOVEMENT_ENABLED_DEFAULT;

    // Variables storing last state of gamepad buttons
    private double lastRuntime = 0.0;

    public ManualOpMode(Constants.Team initTeam) {
        team = initTeam;
    }

    ///////////////////////////////////////////////////////// OPMODE METHODS /////////////////////////////////////////////////////////
    @Override
    public void init() {
        telemetry = new MultipleTelemetry(telemetry, dashboard.getTelemetry());

        telemetry.addData("Initializing...", "");

        movement = new Movement(
                telemetry,
                runtime,
                hardwareMap.get(DcMotor.class, "front_left_drive"),
                hardwareMap.get(DcMotor.class, "front_right_drive"),
                hardwareMap.get(DcMotor.class, "back_left_drive"),
                hardwareMap.get(DcMotor.class, "back_right_drive"),
                false,
                MOVEMENT_BRAKE
        );
        telemetry.addData("Movement", "Initialized");

        gamepad = new GamepadController(
          telemetry,
          runtime,
          gamepad1
        );
        telemetry.addData("Gamepad", "Initialized");

        /* cameraUtils = new CameraUtils(
                telemetry,
                runtime,
                hardwareMap.get(WebcamName.class, "Webcam 1"),
                hardwareMap.appContext.getResources().getIdentifier("tfodMonitorViewId", "id", hardwareMap.appContext.getPackageName())
        ); */
        telemetry.addData("Camera", "Initialized");

        intake = new Intake(
                telemetry,
                runtime,
                hardwareMap.get(DcMotorEx.class, "intake_motor"),
                hardwareMap.get(NormalizedColorSensor.class, "color"),
                hardwareMap.get(NormalizedColorSensor.class, "color2")
                );
        telemetry.addData("Intake", "Initialized");

        lift = new Lift(
                telemetry,
                runtime,
                hardwareMap.get(DcMotorEx.class, "lift_motor"),
                hardwareMap.get(Servo.class, "lift_servo"),
                hardwareMap.get(Servo.class, "lock_servo")
        );
        telemetry.addData("Lift", "Initialized");

        dropduck = new DropDuck(
                telemetry,
                runtime,
                team,
                hardwareMap.get(DcMotorEx.class, "duck_motor")
        );
        telemetry.addData("DropDuck", "Initialized");

        telemetry.addLine();
        telemetry.addLine("--- INFO ---");
        telemetry.addData("Team", team);

        telemetry.update();
    }

    @Override
    public void init_loop() {
    }

    @Override
    public void start() {
        runtime.reset();
    }
    ///////////////////////////////////////////////////////// MAIN LOOP /////////////////////////////////////////////////////////
    @Override
    public void loop() {
        /* MOVEMENT STUFF */
        movement.reset();

        // Translation : joystick (fast) and dpad (slow)
        movement.joystickTranslate(gamepad1);
        movement.dpadTranslate(gamepad1);
        // Rotation : bumpers (fast) and triggers (slow)
        movement.bumperTurn(gamepad1);

        /* DUCK */
        if (gamepad.press(GamepadController.Button.B)) {
            dropduck.toggleDuck();
            if (!MOVEMENT_BRAKE) {
                movement.setBrakeMode(dropduck.getDuck());
            }
        }

        /* INTAKE */
        if (isWaitingForLiftReset && lift.isAtPosition(Lift.Position.DEFAULT)) {
            /* try {
                sleep(4000);
            } catch (InterruptedException e) {
                e.printStackTrace();
            } */
            intake.setIntake(true);
            isWaitingForLiftReset = false;
        }
        if (gamepad.press(GamepadController.Button.A)) {
            if (lift.isAtPosition(Lift.Position.DEFAULT)) {
                intake.cancelRejectForTime();
                intake.toggleIntake();
            } else {
                //intake.setIntake(false);
                lift.goToPosition(Lift.Position.DEFAULT);
                isWaitingForLiftReset = true;
            }
        }
        if (gamepad.longPress(GamepadController.Button.A)) {
            intake.rejectForTime();
        }

        if (
                automationEnabled
                        && intake.getIntake()
                        && !hasProcessedObject
                        && intake.hasObject() // TODO: Switch to intake.hasObjectConsistent();
        ) {
            hasProcessedObject = true;
            intake.rejectForTime();
            lift.cancelResetPosition();
            if (intake.getCurrentObject() == Intake.IntakeObject.BALL) {
                lift.goToPosition(Lift.Position.HUB_HIGH);
            } else if (intake.getCurrentObject() == Intake.IntakeObject.CUBE) {
                lift.goToPosition(cubeLiftPosition);
            }
        } else if (lift.isAtPosition(Lift.Position.DEFAULT)) {
            hasProcessedObject = false;
        }

        /* LIFT */
        if (!lift.getCapstoneMode()) {
            lift.joystickSelect(gamepad1);
            if (gamepad.press(GamepadController.Button.RIGHT_STICK)) {
                intake.rejectForTime();
                lift.cancelResetPosition();
                lift.handleGamepadAction();
            }
            if (gamepad.press(GamepadController.Button.X)) {
                if (automationEnabled) {
                    lift.unlockAndResetPosition(cubeLiftPosition);
                    isWaitingForLiftReset = true;
                } else {
                    lift.unlock();
                }
            }
        } else {
            lift.incrementCapstonePosition((int) (gamepad1.right_stick_y * 10));
            if (gamepad.press(GamepadController.Button.X)) {
                lift.toggleDroppingCapstone();
            }
        }

        /* OTHER */
        if (gamepad.press(GamepadController.Button.Y)) {
            lift.toggleCapstoneModeSafe();
        }
        if (gamepad.press(GamepadController.Button.SHARE)) {
            automationEnabled = !automationEnabled;
        }
        if (gamepad1.options) {
            if (gamepad1.dpad_up) {
                cubeLiftPosition = Lift.Position.HUB_HIGH;
            } else if (gamepad1.dpad_left || gamepad1.dpad_right) {
                cubeLiftPosition = Lift.Position.HUB_MIDDLE;
            } else if (gamepad1.dpad_down) {
                cubeLiftPosition = Lift.Position.HUB_LOW;
            }
        }

        /* APPLY */
        if (!automationEnabled) {
            telemetry.addLine("!!! AUTOMATION DISABLED !!!");
        }
        if (movementEnabled) {
            movement.apply();
        }
        lift.apply();
        dropduck.apply();
        intake.apply();

        /* CHORES */
        lastRuntime = runtime.time();
        telemetry.addLine();
        telemetry.addLine("--- MANUAL ---");
        telemetry.addData("Automation", automationEnabled ? "ENABLED" : "DISABLED");
        telemetry.addData("Mode", lift.getCapstoneMode() ? "CAPSTONE" : cubeLiftPosition);
        telemetry.addData("Remaining time", String.format("%d%s", (int)(120 - runtime.time()), runtime.time() > 90 ? " (ENDGAME)" : ""));
        if (DEBUG) {
            telemetry.addData("hasProcessedObject", hasProcessedObject);
            telemetry.addData("Movement enabled", movementEnabled);
            telemetry.addData("Loop duration", lastRuntime - runtime.time());
        }
        telemetry.update();

    }

    ///////////////////////////////////////////////////////// STOP METHOD /////////////////////////////////////////////////////////
    @Override
    public void stop() {

    }
}
