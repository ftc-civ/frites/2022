package org.firstinspires.ftc.teamcode.manual.opmode;

import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

import org.firstinspires.ftc.teamcode.manual.ManualOpMode;
import org.firstinspires.ftc.teamcode.robot.Constants;

@TeleOp(name="Manual Blue", group="Main")
public class Manual_Blue extends ManualOpMode {

    public Manual_Blue() {
        super(Constants.Team.BLUE);
        this.msStuckDetectInit = 30000;
    }
}
