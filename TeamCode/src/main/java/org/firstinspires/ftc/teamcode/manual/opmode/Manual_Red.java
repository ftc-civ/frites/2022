package org.firstinspires.ftc.teamcode.manual.opmode;

import com.qualcomm.robotcore.eventloop.opmode.TeleOp;

import org.firstinspires.ftc.robotcore.external.Const;
import org.firstinspires.ftc.teamcode.manual.ManualOpMode;
import org.firstinspires.ftc.teamcode.robot.Constants;

@TeleOp(name="Manual Red", group="Main")
public class Manual_Red extends ManualOpMode {

    public Manual_Red() {
        super(Constants.Team.RED);
        this.msStuckDetectInit = 30000;
    }

}
