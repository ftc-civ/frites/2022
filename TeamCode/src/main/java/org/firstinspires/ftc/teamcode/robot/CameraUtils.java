/* Copyright (c) 2017 FIRST. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted (subject to the limitations in the disclaimer below) provided that
 * the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this list
 * of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice, this
 * list of conditions and the following disclaimer in the documentation and/or
 * other materials provided with the distribution.
 *
 * Neither the name of FIRST nor the names of its contributors may be used to endorse or
 * promote products derived from this software without specific prior written permission.
 *
 * NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE GRANTED BY THIS
 * LICENSE. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.firstinspires.ftc.teamcode.robot;

import com.acmerobotics.dashboard.FtcDashboard;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.hardware.TouchSensor;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.robotcore.external.ClassFactory;
import org.firstinspires.ftc.robotcore.external.Telemetry;
import org.firstinspires.ftc.robotcore.external.hardware.camera.Camera;
import org.firstinspires.ftc.robotcore.external.hardware.camera.WebcamName;
import org.firstinspires.ftc.robotcore.external.navigation.VuforiaLocalizer;
import org.firstinspires.ftc.robotcore.external.tfod.Recognition;
import org.firstinspires.ftc.robotcore.external.tfod.TFObjectDetector;
import org.firstinspires.ftc.teamcode.test.OpenCvTest;
import org.opencv.core.Core;
import org.opencv.core.Mat;
import org.opencv.core.Point;
import org.opencv.core.Scalar;
import org.opencv.imgproc.Imgproc;
import org.openftc.easyopencv.OpenCvCamera;
import org.openftc.easyopencv.OpenCvCameraFactory;
import org.openftc.easyopencv.OpenCvCameraRotation;
import org.openftc.easyopencv.OpenCvPipeline;
import org.openftc.easyopencv.OpenCvWebcam;

import java.util.List;

public class CameraUtils
{
    private ElapsedTime runtime;

    private Telemetry telemetry;

    private OpenCvWebcam webcam;

    private Constants.Team team;
    private Constants.StartPosition startPosition;

    private int[] debugArr = new int[5];

    private boolean previewFilter = true;

    public enum CameraPosition {
        UNKNOWN,
        HIGH,
        MIDDLE,
        LOW
    }
    public CameraPosition currentCameraPosition = CameraPosition.UNKNOWN;

    /* public static double BLUE_LOW_LEFT_LIMIT = 1/3;
    public static double BLUE_LOW_RIGHT_LIMIT = 2/3;
    public static double BLUE_HIGH_LEFT_LIMIT = 1/3;
    public static double BLUE_HIGH_RIGHT_LIMIT = 2/3;
    public static double RED_LOW_LEFT_LIMIT = 1/3;
    public static double RED_LOW_RIGHT_LIMIT = 2/3;
    public static double RED_HIGH_LEFT_LIMIT = 1/3;
    public static double RED_HIGH_RIGHT_LIMIT = 2/3; */

    public CameraUtils(Telemetry globalTelemetry, ElapsedTime globalRuntime, Constants.Team globalTeam, Constants.StartPosition globalStartPosition, WebcamName camera, int viewId) {
        // INITIALIZE TELEMETRY
        telemetry = globalTelemetry;
        runtime = globalRuntime;

        team = globalTeam;
        startPosition = globalStartPosition;

        webcam = OpenCvCameraFactory.getInstance().createWebcam(camera, viewId);
    }

    /* private double getLeftLimit() {
        if (team == Constants.Team.BLUE) {
            if (startPosition == Constants.StartPosition.LOW) {
                return BLUE_LOW_LEFT_LIMIT;
            } else {
                return BLUE_HIGH_LEFT_LIMIT;
            }
        } else {
            if (startPosition == Constants.StartPosition.LOW) {
                return RED_LOW_LEFT_LIMIT;
            } else {
                return RED_HIGH_LEFT_LIMIT;
            }
        }
    }
    private double getRightLimit() {
        if (team == Constants.Team.BLUE) {
            if (startPosition == Constants.StartPosition.LOW) {
                return BLUE_LOW_RIGHT_LIMIT;
            } else if (startPosition == Constants.StartPosition.HIGH) {
                return BLUE_HIGH_RIGHT_LIMIT;
            }
        } else {
            if (startPosition == Constants.StartPosition.LOW) {
                return RED_LOW_RIGHT_LIMIT;
            } else if (startPosition == Constants.StartPosition.HIGH) {
                return RED_HIGH_RIGHT_LIMIT;
            }
        }
        return 0;
    } */


    public void startWebcam() {
        webcam.setMillisecondsPermissionTimeout(2500); // Timeout for obtaining permission is configurable. Set before opening.
        webcam.openCameraDeviceAsync(new OpenCvCamera.AsyncCameraOpenListener()
        {
            @Override
            public void onOpened()
            {
                webcam.startStreaming(320, 240, OpenCvCameraRotation.UPRIGHT);
                FtcDashboard.getInstance().startCameraStream(webcam, 10);
            }

            @Override
            public void onError(int errorCode)
            {
                // TODO
            }
        });
    }

    public void setInitPipeline() {
        webcam.setPipeline(new CameraUtils.InitPipeline());
    }

    public void setMainPipeline() {
        webcam.setPipeline(new CameraUtils.MainPipeline());
    }

    public void stopWebcam() {
        webcam.closeCameraDevice();
    }

    public CameraPosition getCameraPosition() {
        return currentCameraPosition;
    }

    public Lift.Position getLiftPosition() {
        if (currentCameraPosition == CameraPosition.HIGH) {
            return Lift.Position.HUB_HIGH;
        } else if (currentCameraPosition == CameraPosition.MIDDLE) {
            return Lift.Position.HUB_MIDDLE;
        } else if (currentCameraPosition == CameraPosition.LOW) {
            return Lift.Position.HUB_LOW;
        }
        return null;
    }

    public void setPreviewFilter(boolean val) {
        previewFilter = val;
    }
    public boolean togglePreviewFilter() {
        previewFilter = !previewFilter;
        return previewFilter;
    }

    class InitPipeline extends OpenCvPipeline {

        Mat hsv = new Mat();
        Mat mask1 = new Mat();
        Mat cropped = new Mat();

        Mat labels = new Mat();
        Mat stats = new Mat();
        Mat centroids = new Mat();

        @Override
        public Mat processFrame(Mat input) {
            hsv.release(); // Pour éviter les memory leaks
            mask1.release();
            cropped.release();

            Imgproc.cvtColor(input, hsv, Imgproc.COLOR_RGB2HSV);
            if (previewFilter) {
                Core.inRange(hsv, new Scalar(15, 100, 100), new Scalar(30, 255, 255), mask1);
                // Core.inRange(hsv, new Scalar(15, 100, 100), new Scalar(30, 255, 255), mask1);
            }
            Core.bitwise_and(input, input, cropped, mask1);

            Imgproc.connectedComponentsWithStats(mask1, labels, stats, centroids);

            double maxCenterPoint = 0;
            int maxArea = 0;
            for (int i = 1; i < stats.rows(); i++) {
                Mat objectCenter = centroids.row(i);
                Mat objectStats = stats.row(i);
                double[] centerPointArr = new double[2];
                int[] statsPointArr = new int[5];
                objectCenter.get(0, 0, centerPointArr);
                objectStats.get(0, 0, statsPointArr);
                if (statsPointArr[4] > 200) {
                    Point centerPoint =  new Point(centerPointArr[0], centerPointArr[1]);
                    Imgproc.circle(cropped, centerPoint, 3, new Scalar(255, 0, 0), 3);
                    if (statsPointArr[4] > maxArea) {
                        maxCenterPoint = centerPointArr[0];
                        maxArea = statsPointArr[4];
                    }
                }
            }

            if (maxArea == 0) {
                currentCameraPosition = CameraPosition.UNKNOWN;
            } else if (maxCenterPoint < input.cols() * 1/3) {
                currentCameraPosition = CameraPosition.LOW;
            } else if (maxCenterPoint > input.cols() * 2/3) {
                currentCameraPosition = CameraPosition.HIGH;
            } else {
                currentCameraPosition = CameraPosition.MIDDLE;
            }

            Imgproc.rectangle(
                    cropped,
                    new Point(0, 0),
                    new Point(input.cols() * 1/3 - 2, input.rows()),
                    currentCameraPosition == CameraPosition.LOW ? new Scalar(255, 0, 0) : new Scalar(255, 255, 0),
                    4);
            Imgproc.rectangle(
                    cropped,
                    new Point(input.cols() * 1/3 + 2, 0),
                    new Point(input.cols() * 2/3 - 2, input.rows()),
                    currentCameraPosition == CameraPosition.MIDDLE ? new Scalar(255, 0, 0) : new Scalar(255, 255, 0),
                    4);
            Imgproc.rectangle(
                    cropped,
                    new Point(input.cols() * 2/3 + 2, 0),
                    new Point(input.cols(), input.rows()),
                    currentCameraPosition == CameraPosition.HIGH ? new Scalar(255, 0, 0) : new Scalar(255, 255, 0),
                    4);

            return cropped;
        }
    }

    class MainPipeline extends OpenCvPipeline {
        Mat hsv = new Mat();
        Mat mask1 = new Mat();
        Mat cropped = new Mat();

        Mat labels = new Mat();
        Mat stats = new Mat();
        Mat centroids = new Mat();

        @Override
        public Mat processFrame(Mat input) {
            hsv.release(); // Pour éviter les memory leaks
            mask1.release();
            cropped.release();

            Imgproc.cvtColor(input, hsv, Imgproc.COLOR_RGB2HSV);
            Core.inRange(hsv, new Scalar(15, 100, 100), new Scalar(30, 255, 255), mask1);
            Core.bitwise_and(input, input, cropped, mask1);

            Imgproc.connectedComponentsWithStats(mask1, labels, stats, centroids);

            double maxCenterPoint = 0;
            int maxArea = 0;
            for (int i = 1; i < stats.rows(); i++) {
                Mat objectCenter = centroids.row(i);
                Mat objectStats = stats.row(i);
                double[] centerPointArr = new double[2];
                int[] statsPointArr = new int[5];
                objectCenter.get(0, 0, centerPointArr);
                objectStats.get(0, 0, statsPointArr);
                if (statsPointArr[4] > 200) {
                    Point centerPoint =  new Point(centerPointArr[0], centerPointArr[1]);
                    Imgproc.circle(cropped, centerPoint, 3, new Scalar(255, 0, 0), 3);
                    if (statsPointArr[4] > maxArea) {
                        maxCenterPoint = centerPointArr[0];
                        maxArea = statsPointArr[4];
                    }
                }
            }

            if (maxArea == 0) {
                currentCameraPosition = CameraPosition.UNKNOWN;
            } else if (maxCenterPoint < input.cols() * 1/3) {
                currentCameraPosition = CameraPosition.LOW;
            } else if (maxCenterPoint > input.cols() * 2/3) {
                currentCameraPosition = CameraPosition.HIGH;
            } else {
                currentCameraPosition = CameraPosition.MIDDLE;
            }

            return cropped;
        }
    }

    public void apply() {
        telemetry.addLine();
        telemetry.addLine("--- CAMERA ---");
        telemetry.addData("Current camera position", currentCameraPosition);
    }

}
