package org.firstinspires.ftc.teamcode.robot;

public class Constants {
  public enum Team {
    RED,
    BLUE
  }

  public enum StartPosition {
    HIGH,
    LOW,
    UNKNOWN
  }
}
