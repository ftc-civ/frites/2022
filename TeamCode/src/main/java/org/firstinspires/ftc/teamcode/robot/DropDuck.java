package org.firstinspires.ftc.teamcode.robot;

import com.acmerobotics.dashboard.config.Config;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorEx;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.robotcore.external.Telemetry;

@Config
public class DropDuck {
    // public static double DUCK_MOTOR_POWER = 0.5;
    public static double DUCK_MOTOR_VELOCITY = 1750;

    public static boolean DEBUG = false;

    private ElapsedTime runtime;
    private Telemetry telemetry;
    private DcMotorEx duckMotor;
    private Constants.Team team;
    private boolean duckMotorOn = false;

    public DropDuck(Telemetry globalTelemetry, ElapsedTime globalRuntime, Constants.Team globalTeam, DcMotorEx duckMotorGlobal) {
        duckMotor = duckMotorGlobal;
        telemetry = globalTelemetry;
        runtime = globalRuntime;
        team = globalTeam;

        duckMotor.setZeroPowerBehavior(DcMotor.ZeroPowerBehavior.BRAKE);
    }

    public void setDuck(boolean value) {
        duckMotorOn = value;
    }

    public boolean getDuck() { return duckMotorOn; }

    public void toggleDuck() {
        duckMotorOn = !duckMotorOn;
    }

    public void apply() {
        // Motor
        if (duckMotorOn) {
            //duckMotor.setPower(DUCK_MOTOR_POWER);
            duckMotor.setVelocity(team == Constants.Team.RED ? -DUCK_MOTOR_VELOCITY : DUCK_MOTOR_VELOCITY);
        } else {
            duckMotor.setVelocity(0);
        }

        // Telemetry
        telemetry.addLine();
        telemetry.addLine("--- DUCK ---");
        if (DEBUG) {
            telemetry.addData("Motor State", duckMotorOn);
            //telemetry.addData("Motor power (const)", DUCK_MOTOR_POWER);
            telemetry.addData("Motor Speed (motor)", duckMotor.getVelocity());
        } else {
            telemetry.addData("Duck state", duckMotorOn ? "ON" : "OFF");
        }
    }
}
