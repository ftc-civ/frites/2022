package org.firstinspires.ftc.teamcode.robot;

import android.graphics.Color;

import com.acmerobotics.dashboard.config.Config;
import com.qualcomm.robotcore.hardware.ColorSensor;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorEx;
import com.qualcomm.robotcore.hardware.DcMotorSimple;
import com.qualcomm.robotcore.hardware.Gamepad;
import com.qualcomm.robotcore.hardware.NormalizedColorSensor;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.robotcore.external.Telemetry;

@Config
public class Intake {
    public static double MOTOR_POWER = 1.0;
    public static DcMotorSimple.Direction MOTOR_DIRECTION = DcMotorSimple.Direction.FORWARD;
    public static DcMotorSimple.Direction MOTOR_REJECT_DIRECTION = DcMotorSimple.Direction.REVERSE;
    public static double DEFAULT_REJECTION_TIME = 1.0;
    public static double DEFAULT_OJBECT_CONSISTENT_TIME = 0.1;

    public static int BALL_RED_LOWERBOUND = 800;
    public static int BALL_GREEN_LOWERBOUND = 1000;
    public static int BALL_BLUE_LOWERBOUND = 500;
    public static int CUBE_RED_LOWERBOUND = 150;
    public static int CUBE_GREEN_LOWERBOUND = 180;
    public static int CUBE_BLUE_HIGHERBOUND = 500;

    public static boolean DEBUG = false;
    
    private boolean motorState;
    private double motorReverseStartTime;
    private double motorReverseTime;
    private boolean motorReject;
    private DcMotorEx intakeMotor;
    private Telemetry telemetry;
    private ElapsedTime runtime;
    private NormalizedColorSensor colorSensor;
    private NormalizedColorSensor colorSensor2;

    public enum IntakeObject {
        BALL,
        CUBE,
        NONE,
        UNKNOWN
    }
    private IntakeObject currentObject = IntakeObject.UNKNOWN;
    private IntakeObject lastObject = IntakeObject.UNKNOWN;
    private double lastObjectChangeTime = 0;



    public Intake(Telemetry globalTelemetry, ElapsedTime globalRuntime, DcMotorEx globalIntakeMotor, NormalizedColorSensor globalColorSensor, NormalizedColorSensor globalColorSensor2) {
        telemetry = globalTelemetry;
        runtime = globalRuntime;
        intakeMotor = globalIntakeMotor;
        colorSensor = globalColorSensor;
        colorSensor2 = globalColorSensor2;

        intakeMotor.setDirection(MOTOR_DIRECTION);
    }

    public void setIntake(boolean value) {
        motorState = value;
    }

    public boolean getIntake() {
        return motorState;
    }

    public void toggleIntake() {
        motorState = !motorState;
    }

    public void setReject(boolean reject) {
        motorReject = reject;
    }

    public void rejectForTime(double time) {
        motorReverseStartTime = runtime.time();
        motorReverseTime = time;
        motorReject = true;
    }
    public void rejectForTime() {
        rejectForTime(DEFAULT_REJECTION_TIME);
    }

    public void cancelRejectForTime() {
        motorReverseTime = 0;
        motorReject = false;
    }

    public boolean isRejectingForTime() {
        return motorReverseTime != 0;
    }

    public void apply() {
        // Motor
        if (motorState) {
            if (motorReject) intakeMotor.setDirection(MOTOR_REJECT_DIRECTION);
            else intakeMotor.setDirection(MOTOR_DIRECTION);
            intakeMotor.setPower(MOTOR_POWER);
        } else {
            intakeMotor.setPower(0);
        }

        if (motorReverseTime != 0 && runtime.time() - motorReverseStartTime > motorReverseTime) {
            motorReverseTime = 0;
            motorReject = false;
            motorState = false;
        }

        // Sensor
        lastObject = currentObject;
        final float[] hsvValues = new float[3];
        final float[] hsvValues2 = new float[3];
        Color.colorToHSV(colorSensor.getNormalizedColors().toColor(), hsvValues);
        Color.colorToHSV(colorSensor2.getNormalizedColors().toColor(), hsvValues2);

        if (hsvValues[0] < 50 || hsvValues[1] < 0.3) {
            currentObject = IntakeObject.NONE;
        } else if (hsvValues[0] > 50 && hsvValues[0] < 100) {
            currentObject = IntakeObject.CUBE;
        } else if (hsvValues[0] > 100) {
            currentObject = IntakeObject.BALL;
        } else {
            currentObject = IntakeObject.UNKNOWN;
        }

        if (currentObject == IntakeObject.NONE || currentObject == IntakeObject.UNKNOWN) {
            if (hsvValues2[0] < 50 || hsvValues2[1] < 0.3) {
                currentObject = IntakeObject.NONE;
            } else if (hsvValues2[0] > 50 && hsvValues2[0] < 100) {
                currentObject = IntakeObject.CUBE;
            } else if (hsvValues2[0] > 100) {
                currentObject = IntakeObject.BALL;
            } else {
                currentObject = IntakeObject.UNKNOWN;
            }
        }

        if (currentObject != lastObject) {
            lastObjectChangeTime = runtime.time();
        }

        // Telemetry
        telemetry.addLine();
        telemetry.addLine("--- INTAKE ---");
        if (DEBUG) {
            telemetry.addData("Motor State", motorState);
            telemetry.addData("Motor is rejecting", motorReject);
            telemetry.addData("Motor direction (const)", MOTOR_DIRECTION);
            telemetry.addData("Motor power (const)", MOTOR_POWER);
            telemetry.addData("Color hue", hsvValues[0]);
            telemetry.addData("Color saturation", hsvValues[1]);
            telemetry.addData("Color value", hsvValues[2]);
            telemetry.addData("Color hue 2", hsvValues2[0]);
            telemetry.addData("Color saturation 2", hsvValues2[1]);
            telemetry.addData("Color value 2", hsvValues2[2]);
            telemetry.addData("Color red", colorSensor.getNormalizedColors().red);
            telemetry.addData("Color green", colorSensor.getNormalizedColors().green);
            telemetry.addData("Color blue", colorSensor.getNormalizedColors().blue);
            telemetry.addData("Color alpha", colorSensor.getNormalizedColors().alpha);
            telemetry.addData("Intake object", currentObject);
            telemetry.addData("Last object", lastObject);
        } else {
            telemetry.addData("Intake state", motorState ? (motorReject ? "REVERSE" : "ON") : "OFF");
            telemetry.addData("Current object", currentObject);
        }
    }


    /* SENSOR STUFF */
    public IntakeObject getCurrentObject() {
        return currentObject;
    }

    public boolean hasObject() {
        return currentObject == IntakeObject.BALL || currentObject == IntakeObject.CUBE;
    }

    public boolean hasObjectConsistent(double time) {
        return (currentObject == IntakeObject.BALL || currentObject == IntakeObject.CUBE) && (lastObjectChangeTime - runtime.time() > time);
    }
    public boolean hasObjectConsistent() {
        return hasObjectConsistent(DEFAULT_OJBECT_CONSISTENT_TIME);
    }

    public IntakeObject getLastObject() {
        return lastObject;
    }
}
