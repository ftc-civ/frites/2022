package org.firstinspires.ftc.teamcode.robot;

import com.acmerobotics.dashboard.config.Config;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.hardware.DcMotorEx;
import com.qualcomm.robotcore.hardware.Gamepad;
import com.qualcomm.robotcore.hardware.Servo;
import com.qualcomm.robotcore.util.ElapsedTime;

import org.firstinspires.ftc.robotcore.external.Telemetry;

import java.util.concurrent.locks.Lock;

@Config
public class Lift {
    public static double DEFAULT_SERVO_POSITION = 1;
    public static double HUB_LOW_SERVO_POSITION = 0.35;
    public static double HUB_MIDDLE_SERVO_POSITION = 0.4;
    public static double HUB_HIGH_SERVO_POSITION = 0.45;
    public static double SHARED_SERVO_POSITION = 0.4;
    public static double CAPSTONE_LOW_SERVO_POSITION = 0.10;
    public static double CAPSTONE_HIGH_SERVO_POSITION = 0.6;

    public static int DEFAULT_LIFT_POSITION = 0;
    public static int HUB_LOW_LIFT_POSITION = -100;
    public static int HUB_MIDDLE_LIFT_POSITION = -350;
    public static int HUB_HIGH_LIFT_POSITION = -675;
    public static int SHARED_LIFT_POSITION = -100;

    public static int LIFT_ROTATION_MINIMUM_POSITION = 500;
    public static int LIFT_ROTATION_TARGET_POSITION = -550;
    public static int LIFT_POSITION_ERROR = 20;
    public static int LIFT_CAPSTONE_MINIMUM_ROTATION_POSITION = 300;
    public static int LIFT_CAPSTONE_MINIMUM_LOCK_POSITION = 400;
    public static int LIFT_CAPSTONE_MINIMUM_LOCK_MIDDLE_POSITION = 10;

    public static double LOCK_CLOSED_POSITION = 1.0;
    public static double LOCK_OPEN_POSITION = 0.2;
    public static double LOCK_OPEN_HIGH_POSITION = 0.0;
    public static double LOCK_CAPSTONE_HIGH_POSITION = 0.50;
    public static double LOCK_CAPSTONE_MIDDLE_POSITION = 0.8;
    public static double LOCK_CAPSTONE_LOW_POSITION = 0.58;
    public static double LOCK_CAPSTONE_DROP_POSITION = 0.0;
    public static double CAPSTONE_MIN_POSITION = 0;
    public static double CAPSTONE_MAX_POSITION = 700;

    public static double GAMEPAD_JOYSTICK_LIMIT = 0.7;

    public static boolean NEGATIVE_ENCODER_VALUES = true;

    public static double RESET_POSITION_DEFAULT_TIME = 1.5;
    public static double RESET_POSITION_LOW_TIME = 0.75;
    public static double RESET_POSITION_MIDDLE_TIME = 1.5;
    public static double RESET_POSITION_HIGH_TIME = 1.5;

    public enum Position {
        DEFAULT(0, DEFAULT_SERVO_POSITION, DEFAULT_LIFT_POSITION, LOCK_OPEN_POSITION),
        HUB_LOW(1, HUB_LOW_SERVO_POSITION, HUB_LOW_LIFT_POSITION, LOCK_OPEN_POSITION),
        HUB_MIDDLE(2, HUB_MIDDLE_SERVO_POSITION, HUB_MIDDLE_LIFT_POSITION, LOCK_OPEN_POSITION),
        HUB_HIGH(3, HUB_HIGH_SERVO_POSITION, HUB_HIGH_LIFT_POSITION, LOCK_OPEN_HIGH_POSITION),
        SHARED(4, SHARED_SERVO_POSITION, SHARED_LIFT_POSITION, LOCK_OPEN_POSITION);

        public int value;
        public double servoPosition;
        public int liftPosition;
        public double openLockPosition;

        Position(int value, double servoPosition, int liftPosition, double openLockPosition) {
            this.value = value;
            this.servoPosition = servoPosition;
            this.liftPosition = liftPosition;
            this.openLockPosition = openLockPosition;
        }
    }
    public enum LockState {
        CLOSED,
        OPEN
    }

    public static Position CAPSTONE_ENABLE_TARGET_POSITION = Position.HUB_MIDDLE;
    public static Position CAPSTONE_DISABLE_TARGET_POSITION = Position.DEFAULT;

    public static boolean DEBUG = false;

    private DcMotorEx liftMotor;
    private Servo liftServo;
    private Servo lockServo;
    private ElapsedTime runtime;
    private Telemetry telemetry;

    private Position targetPosition = Position.DEFAULT;
    private Position currentServoPosition = Position.DEFAULT;
    private Position currentLiftPosition = Position.DEFAULT;
    private LockState targetLockState = LockState.CLOSED;
    private LockState currentLockState = LockState.CLOSED;
    private boolean isMoving = false;

    private double resetPositionStartTime;
    private double resetPositionTime;
    private boolean resetPosition = false;

    private boolean isCapstoneMode = false;
    private int capstoneLiftPosition = 0;
    private boolean isDroppingCapstone = false;

    private boolean capstoneModeToBeEnabled = false;

    private Position lastGamepadPosition = Position.DEFAULT;

    public Lift(Telemetry globalTelemetry, ElapsedTime globalRuntime, DcMotorEx globalLiftMotor, Servo globalLiftServo, Servo globalLockServo) {
        telemetry = globalTelemetry;
        runtime = globalRuntime;
        liftMotor = globalLiftMotor;
        liftServo = globalLiftServo;
        lockServo = globalLockServo;

        liftMotor.setMode(DcMotor.RunMode.STOP_AND_RESET_ENCODER);
        liftServo.resetDeviceConfigurationForOpMode();
    }

    // Set lock to open when targetPosition is reached
    public void unlock() {
        targetLockState = LockState.OPEN;
    }

    public void lock() { targetLockState = LockState.CLOSED; }

    public void unlockAndResetPosition(double time) {
        targetLockState = LockState.OPEN;
        resetPositionStartTime = runtime.time();
        resetPositionTime = time;
        resetPosition = true;
    }
    public void unlockAndResetPosition() {
        unlockAndResetPosition(RESET_POSITION_DEFAULT_TIME);
    }
    public void unlockAndResetPosition(Position mode) {
        switch (mode) {
            case HUB_HIGH:
                unlockAndResetPosition(RESET_POSITION_HIGH_TIME);
                break;
            case HUB_MIDDLE:
                unlockAndResetPosition(RESET_POSITION_MIDDLE_TIME);
                break;
            case HUB_LOW:
                unlockAndResetPosition(RESET_POSITION_LOW_TIME);
                break;
            default:
                unlockAndResetPosition(RESET_POSITION_DEFAULT_TIME);
                break;
        }
    }

    // Go to a certain position
    public void goToPosition(Position position) {
        isMoving = true;
        targetPosition = position;
        targetLockState = LockState.CLOSED;
    }

    // Check if currently close to position
    public boolean isAtPosition(Position position) {
        return (
            !isMoving
            && currentLiftPosition == position
            && currentServoPosition == position
        );
    }
    public boolean isAtTargetPosition() {
        return isAtPosition(targetPosition);
    }

    public boolean isAtLockState(LockState lockState) {
        return currentLockState == lockState;
    }
    public boolean isAtTargetLockState() {
        return isAtLockState(targetLockState);
    }
    public void cancelResetPosition() {
        resetPosition = false;
    }

    // Select next position for handleGamepadAction
    public void joystickSelect(Gamepad gamepad) {
        if (gamepad.right_stick_x > GAMEPAD_JOYSTICK_LIMIT) {
            lastGamepadPosition = Position.DEFAULT;
        } else if (gamepad.right_stick_x < -GAMEPAD_JOYSTICK_LIMIT) {
            lastGamepadPosition = Position.HUB_MIDDLE;
        } else if (gamepad.right_stick_y > GAMEPAD_JOYSTICK_LIMIT) {
            lastGamepadPosition = Position.HUB_LOW;
        } else if (gamepad.right_stick_y < - GAMEPAD_JOYSTICK_LIMIT) {
            lastGamepadPosition = Position.HUB_HIGH;
        }
    }

    // Go to position if pressed once, unlock if pressed second time
    public void handleGamepadAction() {
        goToPosition(lastGamepadPosition);
    }

    private double getLiftPosition() {
        if (NEGATIVE_ENCODER_VALUES) {
            return -liftMotor.getCurrentPosition();
        } else {
            return liftMotor.getCurrentPosition();
        }
    }

    public Position getTargetPosition() {
        return targetPosition;
    }

    public boolean getCapstoneMode() {
        return isCapstoneMode;
    }
    public void setCapstoneMode(boolean val) {
        isCapstoneMode = val;
    }
    public boolean toggleCapstoneMode() {
        isCapstoneMode = !isCapstoneMode;
        return isCapstoneMode;
    }
    //
    public void toggleCapstoneModeSafe() {
        isDroppingCapstone = false;
        if (isCapstoneMode || capstoneModeToBeEnabled) {
            goToPosition(Position.DEFAULT);
            isCapstoneMode = false;
            capstoneModeToBeEnabled = false;
        } else {
            capstoneModeToBeEnabled = true;
            goToPosition(Position.HUB_MIDDLE);
        }
    }

    public void setCapstonePosition(int val) {
        capstoneLiftPosition = -val;
    }
    public void incrementCapstonePosition(int val) {
        if (Math.abs(capstoneLiftPosition) - val < CAPSTONE_MIN_POSITION || Math.abs(capstoneLiftPosition - val) > CAPSTONE_MAX_POSITION) {
            return;
        }
        capstoneLiftPosition -= val;
    }

    public void setDroppingCapstone(boolean val) {
        isDroppingCapstone = val;
    }
    public void toggleDroppingCapstone() {
        isDroppingCapstone = !isDroppingCapstone;
    }

    // Apply
    public void apply() {
        if (capstoneModeToBeEnabled && isAtPosition(Position.HUB_MIDDLE)) {
            isCapstoneMode = true;
            capstoneModeToBeEnabled = false;
        }

        if (!isCapstoneMode) {
            // Normal mode : go to target lift and servo position
            if (resetPosition && runtime.time() - resetPositionStartTime > resetPositionTime) {
                goToPosition(Position.DEFAULT);
                resetPosition = false;
                resetPositionTime = 0;
            }

            // Lift & servo
            if (currentServoPosition != targetPosition) {
                if (getLiftPosition() >= LIFT_ROTATION_MINIMUM_POSITION) {
                    liftServo.setPosition(targetPosition.servoPosition);
                    currentServoPosition = targetPosition;
                } else {
                    liftMotor.setTargetPosition(LIFT_ROTATION_TARGET_POSITION);
                }
                isMoving = true;

            } else {
                liftMotor.setTargetPosition(targetPosition.liftPosition);
                liftServo.setPosition(targetPosition.servoPosition);

                isMoving = (Math.abs(getLiftPosition()) <= Math.abs(targetPosition.liftPosition) - LIFT_POSITION_ERROR)
                    || (Math.abs(getLiftPosition()) >= Math.abs(targetPosition.liftPosition) + LIFT_POSITION_ERROR);
                if (!isMoving) {
                    currentLiftPosition = targetPosition;
                }
            }

            // Lock
            // if (isMoving || targetPosition == Position.DEFAULT) {
            //     lockServo.setPosition(LockState.CLOSED.position);
            //     currentLockState = LockState.CLOSED;
            // } else {
            if (targetLockState == LockState.OPEN) {
                lockServo.setPosition(targetPosition.openLockPosition);
            } else {
                lockServo.setPosition(LOCK_CLOSED_POSITION);
            }

            currentLockState = targetLockState;
            // }
        } else {
            liftMotor.setTargetPosition(-capstoneLiftPosition);
            if (Math.abs(capstoneLiftPosition) > LIFT_CAPSTONE_MINIMUM_ROTATION_POSITION) {
                liftServo.setPosition(CAPSTONE_HIGH_SERVO_POSITION);
            } else {
                liftServo.setPosition(CAPSTONE_LOW_SERVO_POSITION);
            }
            if (Math.abs(capstoneLiftPosition) > LIFT_CAPSTONE_MINIMUM_LOCK_POSITION) {
                if (isDroppingCapstone) {
                    lockServo.setPosition(LOCK_CAPSTONE_DROP_POSITION);
                } else {
                    lockServo.setPosition(LOCK_CAPSTONE_HIGH_POSITION);
                }
            } else if (Math.abs(capstoneLiftPosition) > LIFT_CAPSTONE_MINIMUM_LOCK_MIDDLE_POSITION) {
                lockServo.setPosition(LOCK_CAPSTONE_MIDDLE_POSITION);
            } else {
                lockServo.setPosition(LOCK_CAPSTONE_LOW_POSITION);
            }
        }


        liftMotor.setMode(DcMotor.RunMode.RUN_TO_POSITION);
        liftMotor.setPower(1.0);

        // Telemetry
        telemetry.addLine();
        telemetry.addLine("--- LIFT ---");
        if (DEBUG) {
            telemetry.addData("Next gamepad position", lastGamepadPosition);
            telemetry.addData("Target position", targetPosition);
            telemetry.addData("Current lift servo position", currentServoPosition);
            telemetry.addData("Current lift motor position", currentLiftPosition);
            telemetry.addData("Current lift motor value", getLiftPosition());
            telemetry.addData("Target lift motor value", targetPosition.liftPosition);
            telemetry.addData("Lift moving", isMoving);
            telemetry.addData("Is at target position", isAtTargetPosition());
            telemetry.addData("Target lock state", targetLockState);
            telemetry.addData("Current lock state", currentLockState);
            telemetry.addData("Reset position", resetPosition);
            telemetry.addData("Reset position time", resetPositionTime);
            telemetry.addData("Reset position start time", resetPositionStartTime);
            telemetry.addData("Capstone mode", isCapstoneMode);
            telemetry.addData("Capstone lift position", capstoneLiftPosition);
            telemetry.addData("Capstone lift mode", Math.abs(capstoneLiftPosition) > LIFT_CAPSTONE_MINIMUM_ROTATION_POSITION ? "HIGH" : "LOW");
            telemetry.addData("Capstone lock mode", Math.abs(capstoneLiftPosition) > LIFT_CAPSTONE_MINIMUM_LOCK_POSITION ? "HIGH" : "LOW");
            telemetry.addData("Capstone mode to be enabled", capstoneModeToBeEnabled);
        } else {
            if (isAtTargetPosition()) {
                if (targetPosition == lastGamepadPosition) {
                    telemetry.addData("Current position", targetPosition);
                } else {
                    telemetry.addData("Next position", lastGamepadPosition);
                }
            } else {
                telemetry.addData("Target position (moving)", targetPosition);
            }
            if (isAtTargetLockState()) {
                telemetry.addData("Lock state", currentLockState);
            } else {
                telemetry.addData("Target lock state (waiting)", targetLockState);
            }
        }
    }
}
