/* Copyright (c) 2017 FIRST. All rights reserved.
 *
 * Redistribution and use in source and binary forms, with or without modification,
 * are permitted (subject to the limitations in the disclaimer below) provided that
 * the following conditions are met:
 *
 * Redistributions of source code must retain the above copyright notice, this list
 * of conditions and the following disclaimer.
 *
 * Redistributions in binary form must reproduce the above copyright notice, this
 * list of conditions and the following disclaimer in the documentation and/or
 * other materials provided with the distribution.
 *
 * Neither the name of FIRST nor the names of its contributors may be used to endorse or
 * promote products derived from this software without specific prior written permission.
 *
 * NO EXPRESS OR IMPLIED LICENSES TO ANY PARTY'S PATENT RIGHTS ARE GRANTED BY THIS
 * LICENSE. THIS SOFTWARE IS PROVIDED BY THE COPYRIGHT HOLDERS AND CONTRIBUTORS
 * "AS IS" AND ANY EXPRESS OR IMPLIED WARRANTIES, INCLUDING, BUT NOT LIMITED TO,
 * THE IMPLIED WARRANTIES OF MERCHANTABILITY AND FITNESS FOR A PARTICULAR PURPOSE
 * ARE DISCLAIMED. IN NO EVENT SHALL THE COPYRIGHT OWNER OR CONTRIBUTORS BE LIABLE
 * FOR ANY DIRECT, INDIRECT, INCIDENTAL, SPECIAL, EXEMPLARY, OR CONSEQUENTIAL
 * DAMAGES (INCLUDING, BUT NOT LIMITED TO, PROCUREMENT OF SUBSTITUTE GOODS OR
 * SERVICES; LOSS OF USE, DATA, OR PROFITS; OR BUSINESS INTERRUPTION) HOWEVER
 * CAUSED AND ON ANY THEORY OF LIABILITY, WHETHER IN CONTRACT, STRICT LIABILITY,
 * OR TORT (INCLUDING NEGLIGENCE OR OTHERWISE) ARISING IN ANY WAY OUT OF THE USE
 * OF THIS SOFTWARE, EVEN IF ADVISED OF THE POSSIBILITY OF SUCH DAMAGE.
 */

package org.firstinspires.ftc.teamcode.robot;

import com.acmerobotics.dashboard.config.Config;
import com.qualcomm.robotcore.hardware.ColorSensor;
import com.qualcomm.robotcore.hardware.DcMotor;
import com.qualcomm.robotcore.util.Range;
import com.qualcomm.robotcore.util.ElapsedTime;
import com.qualcomm.robotcore.hardware.Gamepad;

import org.firstinspires.ftc.robotcore.external.Telemetry;

@Config
public class Movement
{
    ///////////* CONFIG *///////////
    public static double FRONT_LEFT_COEFF = 1;
    public static double FRONT_RIGHT_COEFF = 1;
    public static double BACK_LEFT_COEFF = 1;
    public static double BACK_RIGHT_COEFF = 1;
    public static double MAX_DPAD_SENSITIVITY = 0.5; // How much the robot moves when dpad is used
    public static double TIME_TO_DPAD_FRONT = 1.5;
    public static double TIME_TO_DPAD_SIDEWAYS = 1;
    public static double DPAD_MARGIN_FRONT = 0.2;
    public static double DPAD_MARGIN_SIDEWAYS = 0.3;
    public static double TICKS_PER_ROTATION = 500; // TODO: Measure this

    public static double TRIGGER_TURN_VALUE = 0.8;
    public static double BUMPER_TURN_VALUE = 0.3;

    public static boolean DEBUG = false;

    public static boolean NO_MOVEMENT_WHEN_TURNING = false;

    ///////////* CLASS PROPERTIES *///////////
    private float turn = 0f;

    private ElapsedTime runtime;

    // Initialize motors
    private DcMotor frontLeftDrive, frontRightDrive, backLeftDrive, backRightDrive, ticksMotor;


    private double frontLeftPower = 0;
    private double frontRightPower = 0;
    private double backLeftPower = 0;
    private double backRightPower = 0;

    private DcMotor.ZeroPowerBehavior dcMoveMode;
    private DcMotor.ZeroPowerBehavior defaultDcMoveMode;

    private boolean isReduced = false;

    private Telemetry telemetry;


    private double firstTurnTicks;
    private boolean rotateLock;
    private double turnAngle, turnSpeed; // Angle in degrees

    private double dpadTime; // The time the dpad has been pressed, to progressively ramp up speed

    ///////////* CONSTRUCTOR *///////////
    public Movement(Telemetry globalTelemetry, ElapsedTime globalRuntime, DcMotor FL, DcMotor FR, DcMotor BL, DcMotor BR, boolean isReducedMain, boolean brakeMode) {
        DcMotor.ZeroPowerBehavior mode;
        if (brakeMode) {
            mode = DcMotor.ZeroPowerBehavior.BRAKE;
        } else {
            mode = DcMotor.ZeroPowerBehavior.FLOAT;
        }
        dcMoveMode = mode;
        defaultDcMoveMode = mode;

        // INITIALIZE TELEMETRY
        telemetry = globalTelemetry;

        runtime = globalRuntime;

        isReduced = isReducedMain;

        // Assign motors from ManualOpMode
        frontLeftDrive  = FL; frontLeftDrive.setDirection(DcMotor.Direction.FORWARD); frontLeftDrive.setZeroPowerBehavior(mode);
        frontRightDrive = FR; frontRightDrive.setDirection(DcMotor.Direction.REVERSE); frontRightDrive.setZeroPowerBehavior(mode);
        backLeftDrive  = BL;  backLeftDrive.setDirection(DcMotor.Direction.FORWARD); backLeftDrive.setZeroPowerBehavior(mode);
        backRightDrive = BR;  backRightDrive.setDirection(DcMotor.Direction.REVERSE); backRightDrive.setZeroPowerBehavior(mode);


        ticksMotor = backLeftDrive;
    }

    ///////////* MOVEMENT CALCULATIONS *///////////
    // Move in a direction indicated by the first two coordinates, and turn
    public void move(double front, double sideways, double turn) {
        backLeftPower   += (front + sideways - turn) * BACK_LEFT_COEFF;
        backRightPower  += (front - sideways + turn) * BACK_RIGHT_COEFF;
        frontRightPower += (front + sideways + turn) * FRONT_RIGHT_COEFF;
        frontLeftPower  += (front - sideways - turn) * FRONT_LEFT_COEFF;
    }

    // Move with an angle and a speed instead
    public void polarMove(double angle, double speed, double turn) {
        move(
                Math.sin(angle) * speed,
                Math.cos(angle) * speed,
                turn
        );
    }

    // Add extra rotation
    public void rotate(double angleInDegrees, double speed) {
        rotateLock = true;
        firstTurnTicks = ticksMotor.getCurrentPosition();
        turnAngle = angleInDegrees; turnSpeed = speed;
    }

    // Add rotation lock
    public void keepRotating() {
        if (turnAngle > 0) {
            if (firstTurnTicks + turnAngle / 360 * TICKS_PER_ROTATION > ticksMotor.getCurrentPosition()) {
                move(0, 0, turnSpeed);
            } else
                rotateLock = false;
        } else {
            if (firstTurnTicks - turnAngle / 360 * TICKS_PER_ROTATION < ticksMotor.getCurrentPosition()) {
                move(0, 0, -turnSpeed);
            } else
                rotateLock = false;
        }
    }

    // Check if rotation lock is enabled
    public boolean checkRotating() {
        return rotateLock;
    }

    // Turns off all motors, full stop
    public void reset(boolean includeTurn) {
        dcMoveMode = defaultDcMoveMode;
        frontLeftPower = 0;
        frontRightPower = 0;
        backLeftPower = 0;
        backRightPower = 0;
        if (includeTurn)
            turn = 0f;
    }
    public void reset() {
        reset(true);
    }

    ///////////* GAMEPAD CONTROL *///////////
    private double smooth(double input) {
        if (Math.abs(input) < 0.1) {
            return 0;
        } else if (Math.abs(input) >= 0.1 || Math.abs(input) < 0.7) {
            return 0.83*input+0.02;
        } else if (Math.abs(input) >= 0.7 || Math.abs(input) < 0.9) {
            return 2*input-0.8;
        } else if (Math.abs(input) >= 0.9) {
            return 1;
        }
        return 0;
    }

    // Translates the robot with the left joystick
    public void joystickTranslate(Gamepad gamepad) {
        double sideways = isReduced ? gamepad.left_stick_x / 2 : gamepad.left_stick_x;
        double front = isReduced ? gamepad.left_stick_y / 2 : gamepad.left_stick_y;
        // Gradual speed increase
        sideways = smooth(sideways);
        front = smooth(front);

        move(
                -front,
                -sideways,
                0
        );
    }

    // Move slowly with the dpad
    public void dpadTranslate(Gamepad gamepad) {
        if (!(gamepad.dpad_up || gamepad.dpad_down || gamepad.dpad_left || gamepad.dpad_right)) {
          dpadTime = runtime.time();
        } // Allows us to get the time the dpad was pressed, so we can progressively augment the speed of the robot
        double time = runtime.time() - dpadTime;
        double sidewaysTime = time/ TIME_TO_DPAD_SIDEWAYS;
        double frontTime = time/ TIME_TO_DPAD_FRONT;
        sidewaysTime += DPAD_MARGIN_SIDEWAYS;
        frontTime += DPAD_MARGIN_FRONT;
        if (sidewaysTime > MAX_DPAD_SENSITIVITY) sidewaysTime = MAX_DPAD_SENSITIVITY;
        if (frontTime > MAX_DPAD_SENSITIVITY) frontTime = MAX_DPAD_SENSITIVITY;
        move(
                -(gamepad.dpad_down? frontTime :0) + (gamepad.dpad_up? frontTime :0),
                -(gamepad.dpad_right? sidewaysTime :0) + (gamepad.dpad_left? sidewaysTime :0),
                0
        );
    }

    // Turn with the bumpers and triggers
    public void bumperTurn(Gamepad gamepad) {
        if (gamepad.left_bumper || gamepad.right_bumper) {
            if (gamepad.left_bumper)
                turn -= BUMPER_TURN_VALUE;
            if (gamepad.right_bumper)
                turn += BUMPER_TURN_VALUE;
        } else {
            turn += gamepad.right_trigger*TRIGGER_TURN_VALUE;
            turn -= gamepad.left_trigger*TRIGGER_TURN_VALUE;
        }
        if (NO_MOVEMENT_WHEN_TURNING && turn != 0) {
            reset(false); // Because Jeremy wanted to
        }
        move(0, 0, turn);
    }

    public void setBrakeMode(boolean val) {
        DcMotor.ZeroPowerBehavior mode;
        if (val) {
            mode = DcMotor.ZeroPowerBehavior.BRAKE;
        } else {
            mode = DcMotor.ZeroPowerBehavior.FLOAT;
        }
        dcMoveMode = mode;
        defaultDcMoveMode = mode;
    }

    ///////////* GET INFO *///////////
    public int getEncoder() {
        return frontLeftDrive.getCurrentPosition();
    }
    public int getEncoderAverage() {
        return (frontLeftDrive.getCurrentPosition()
                + frontRightDrive.getCurrentPosition()
                + backLeftDrive.getCurrentPosition()
                + backRightDrive.getCurrentPosition()) / 4;
    }

    ///////////* APPLY CHANGES *///////////
    // Apply all changes made before
    public void apply() {
        // Motors
        frontLeftDrive.setZeroPowerBehavior(dcMoveMode);
        frontRightDrive.setZeroPowerBehavior(dcMoveMode);
        backLeftDrive.setZeroPowerBehavior(dcMoveMode);
        backRightDrive.setZeroPowerBehavior(dcMoveMode);

        frontLeftDrive.setPower(Range.clip(frontLeftPower, -1.0, 1.0));
        frontRightDrive.setPower(Range.clip(frontRightPower, -1.0, 1.0));
        backLeftDrive.setPower(Range.clip(backLeftPower, -1.0, 1.0));
        backRightDrive.setPower(Range.clip(backRightPower, -1.0, 1.0));



        // Telemetry
        if (DEBUG) {
            telemetry.addLine();
            telemetry.addLine("--- MOVEMENT ---");
            telemetry.addData("Front Left Power", frontLeftPower);
            telemetry.addData("Front Right Power", frontRightDrive);
            telemetry.addData("Back Left Power", backLeftDrive);
            telemetry.addData("Back Right Power", backRightDrive);
            telemetry.addLine();
            telemetry.addData("Motor mode", dcMoveMode);
            telemetry.addData("Default motor mode", defaultDcMoveMode);
            telemetry.addData("Rotation lock", rotateLock);
            telemetry.addData("Reduced mode", isReduced);
            // TODO: Store intenend front, sideways & turn to be able to display;
        } else {
            // Empty
        }
    }


}
