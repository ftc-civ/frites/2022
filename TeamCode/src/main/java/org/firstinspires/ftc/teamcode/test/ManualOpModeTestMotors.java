package org.firstinspires.ftc.teamcode.test;


import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotorEx;
import com.qualcomm.robotcore.util.ElapsedTime;
import java.util.concurrent.TimeUnit;

import org.firstinspires.ftc.teamcode.robot.GamepadController;

@TeleOp(name="Manual Test Motors",group="Test")
public class ManualOpModeTestMotors extends OpMode
{
    public boolean motorsEnabled = true;
    public enum Team {
        BLUE,
        RED
    }
    public Team team = Team.BLUE;
    //////////////////////////////////////////////////////// CLASS MEMBERS /////////////////////////////////////////////////////////
    private GamepadController gamepad;
    private ElapsedTime runtime = new ElapsedTime();
    private DcMotorEx dcmotor;
    private double speed = 0.0;
    private String lastPad = "none";

    ///////////////////////////////////////////////////////// OPMODE METHODS /////////////////////////////////////////////////////////
    @Override
    public void init() {
        dcmotor = hardwareMap.get(DcMotorEx.class, "duck_motor");
        gamepad = new GamepadController(
                telemetry,
                runtime,
                gamepad1
        );
    }

    @Override
    public void init_loop() {
    }

    @Override
    public void start() {
        runtime.reset();
    }
    ///////////////////////////////////////////////////////// MAIN LOOP /////////////////////////////////////////////////////////

    @Override
    public void loop() {


        if (gamepad1.a) speed = 0.0;
        if (gamepad.press(GamepadController.Button.X)){

            if (lastPad!="X") {
                dcmotor.setPower(0);
                try {
                    TimeUnit.MILLISECONDS.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                speed = -1890.0;
                lastPad = "X";
            }else{
                telemetry.addData("DoneX", true);
                speed = 0.0;
                lastPad = "B";
            }


        }
        if (gamepad.press(GamepadController.Button.B)){

            if (lastPad!="B") {
                dcmotor.setPower(0);
                try {
                    TimeUnit.MILLISECONDS.sleep(200);
                } catch (InterruptedException e) {
                    e.printStackTrace();
                }
                speed = 1890.0;
                lastPad = "B";
            }else{
                telemetry.addData("DoneB", true);
                speed = 0.0;
                lastPad = "X";
            }

        }
        speed += gamepad1.left_stick_y;


        ////////////////////// Update ////////////////////////

        dcmotor.setVelocity(speed);

        //////////////////// TELEMETRY ///////////////////////
        telemetry.addData("VitesseVar", speed);
        telemetry.addData("VitesseReal", dcmotor.getVelocity());
        telemetry.addData("Power", dcmotor.getPower());
        telemetry.addData("GamePad", lastPad);
        telemetry.update();

    }

    ///////////////////////////////////////////////////////// STOP METHOD /////////////////////////////////////////////////////////
    @Override
    public void stop() {

    }
}
