package org.firstinspires.ftc.teamcode.test;

import com.qualcomm.robotcore.eventloop.opmode.LinearOpMode;
import com.qualcomm.robotcore.eventloop.opmode.OpMode;
import com.qualcomm.robotcore.eventloop.opmode.TeleOp;
import com.qualcomm.robotcore.hardware.DcMotor;

@TeleOp(name="Motor Tests",group="Test")
public class MotorTests extends LinearOpMode
{
    //////////////////////////////////////////////////////// CLASS MEMBERS /////////////////////////////////////////////////////////
    private DcMotor frontLeftDrive, frontRightDrive, backLeftDrive, backRightDrive;

    private void sleepT(long millis) {
        try {
            Thread.sleep(millis);
        } catch (InterruptedException e) {
            e.printStackTrace();
        }
    }

    ///////////////////////////////////////////////////////// OPMODE METHODS /////////////////////////////////////////////////////////
    @Override
    public void runOpMode() {
        frontLeftDrive = hardwareMap.get(DcMotor.class, "front_left_drive");
        frontRightDrive = hardwareMap.get(DcMotor.class, "front_right_drive");
        backLeftDrive = hardwareMap.get(DcMotor.class, "back_left_drive");
        backRightDrive = hardwareMap.get(DcMotor.class, "back_right_drive");

        frontLeftDrive.setDirection(DcMotor.Direction.FORWARD);
        frontRightDrive.setDirection(DcMotor.Direction.FORWARD);
        backLeftDrive.setDirection(DcMotor.Direction.FORWARD);
        backRightDrive.setDirection(DcMotor.Direction.FORWARD);

        telemetry.addData("Motor:", "front left");
        telemetry.update();
        frontLeftDrive.setPower(1.0);
        sleepT(3000);
        frontLeftDrive.setPower(0.0);
        telemetry.addData("Motor:", "front right");
        telemetry.update();
        frontRightDrive.setPower(1.0);
        sleepT(3000);
        frontRightDrive.setPower(0.0);
        telemetry.addData("Motor:", "back left");
        telemetry.update();
        backLeftDrive.setPower(1.0);
        sleepT(3000);
        backLeftDrive.setPower(0.0);
        telemetry.addData("Motor:", "back right");
        telemetry.update();
        backRightDrive.setPower(1.0);
        sleepT(3000);
        backRightDrive.setPower(0.0);
    }

    ///////////////////////////////////////////////////////// MAIN LOOP /////////////////////////////////////////////////////////
}
