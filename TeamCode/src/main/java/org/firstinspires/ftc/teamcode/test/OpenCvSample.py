import cv2

pos = None
p_color = None
def click_event(event, x, y, flags, params):
    global frame, croped, mask1, pos, p_color

    # checking for left mouse clicks
    if event == cv2.EVENT_LBUTTONDOWN:
        pos = (x,y)
        p_color = str(frame[pos[0], pos[1]])

cap = cv2.VideoCapture(0)
while cap.isOpened():
    ret, frame = cap.read()

    img_hsv = cv2.cvtColor(frame, cv2.COLOR_BGR2HSV)

    mask1 = cv2.inRange(img_hsv, (22, 30, 20), (30, 255, 255))

    ## Merge the mask and crop the yellow regions
    # mask = cv2.bitwise_or(mask1)
    croped = cv2.bitwise_and(frame, frame, mask=mask1)

    num, labels, stats, center = cv2.connectedComponentsWithStats(mask1, connectivity=8)

    # print(stats)

    for a in range(len(stats)):
        i = center[a]
        y = stats[a]
        if y[4] > 500 and y[4] != max([t[4] for t in stats]):
            color = (0, 0, int(y[4]/1000))
            cv2.circle(croped, (int(i[0]), int(i[1])), 3, color, 3)
            cv2.rectangle(croped, (int(y[0]), int(y[1])), (int(y[0] + y[2]), int(y[1] + y[3])), color, 3)
            cv2.putText(croped, str(y[4]), (int(i[0]), int(i[1])), cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)

            # cv2.circle(mask1, (int(i[0]), int(i[1])), 3, color, 3)
            # cv2.rectangle(mask1, (int(y[0]), int(y[1])), (int(y[0] + y[2]), int(y[1] + y[3])), color, 3)
            # cv2.putText(mask1, str(y[4]), (int(i[0]), int(i[1])), cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)

            cv2.circle(frame, (int(i[0]), int(i[1])), 3, color, 3)
            cv2.rectangle(frame, (int(y[0]), int(y[1])), (int(y[0] + y[2]), int(y[1] + y[3])), color, 3)
            cv2.putText(frame, str(y[4]), (int(i[0]), int(i[1])), cv2.FONT_HERSHEY_SIMPLEX, 0.5, color, 2)

    if pos: cv2.putText(frame, p_color, pos, cv2.FONT_HERSHEY_SIMPLEX, 1, (255, 0, 0), 2)

        # if a == 10:
        #     break

    if cv2.waitKey(1) & 0xFF == ord('q'):
        break

    # cv2.imshow('mask', mask1)
    cv2.imshow('croped', croped)
    cv2.imshow('camera', frame)

    cv2.setMouseCallback('camera', click_event)

cap.release()
cv2.destroyAllWindows()
